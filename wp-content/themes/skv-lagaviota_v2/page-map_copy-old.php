<?php /* Template Name: Page Map copy 2 */ get_header(); ?>

    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        
        <section class="container">
            <div class="grid">
                
                <div class="container-map">
                    <div class="media">
                        <img class="media-bg" src="<?php echo get_template_directory_uri(); ?>/assets/images/planol-lagaviota.jpg" alt="Camping La Gaviota map" width="1000" height="663">
                    </div>
                    
                    <div class="hotspots">
                        <div class="hotspot">
                            <div class="trigger"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></div>
                            <div class="content-map">
                                <figure class="">
                                    <img src="http://unsplash.it/1184/666"/>
                                    <figcaption>Torquent mi nullam conubia vestibulum adipiscing</figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="hotspot">
                            <div class="trigger"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></div>
                            <div class="content-map">
                                <figure>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/accommodation/mobilhome-llevant-1.jpg"/ alt="Mobilhome LLevant" width="1400" height="931">
                                    <figcaption>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="hotspot">
                            <div class="trigger"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></div>
                            <div class="content-map">
                                <figure>
                                    <img src="http://unsplash.it/1264/711"/>
                                    <figcaption>Nostra litora dui parturient enim curae</figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
		
            
        <?php get_template_part( 'templates/content', 'reviews' ); ?>
        
        
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

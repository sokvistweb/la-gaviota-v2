<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="keywords" content="Costa Brava, playa, camping, bungalows, mobilhomes" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="author" href="humans.txt">

    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico" rel="shortcut icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,900|Open+Sans:400,700&display=swap" rel="stylesheet">
    <link rel="canonical" href="">


    <!-- Open Graph -->
    <meta property="og:locale" content="es_ES">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Camping La Gaviota">
    <meta property="og:description" content="El camping La Gaviota de Sant Pere Pescador está ubicado en primera línea de la playa más espectacular del golfo de Roses, en la Costa Brava.">
    <meta property="og:image" content="http://sokvist.com/assets/images/sokvist-web-og-image.jpg">
    <meta property="og:url" content="http://sokvist.com/">
    <meta property="og:site_name" content="Sokvist Web">

    <!-- Global site tag (gtag.js) - Google Analytics 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144894094-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-144894094-1');
    </script>-->

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
        
    <header role="banner">
            
    </header>
        

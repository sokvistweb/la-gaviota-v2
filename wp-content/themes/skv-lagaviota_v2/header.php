<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="keywords" content="Costa Brava, playa, camping, bungalows, mobilhomes" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="author" href="humans.txt">

    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico" rel="shortcut icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,900|Open+Sans:400,700&display=swap" rel="stylesheet">
    <link rel="canonical" href="">


    <!-- Open Graph -->
    <meta property="og:locale" content="es_ES">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Camping La Gaviota">
    <meta property="og:description" content="El camping La Gaviota de Sant Pere Pescador está ubicado en primera línea de la playa más espectacular del golfo de Roses, en la Costa Brava.">
    <meta property="og:image" content="http://sokvist.com/assets/images/sokvist-web-og-image.jpg">
    <meta property="og:url" content="http://sokvist.com/">
    <meta property="og:site_name" content="Sokvist Web">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-35322356-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-35322356-1');
    </script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-7YM1GYVKYD"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-7YM1GYVKYD');
	</script>

    <?php wp_head(); ?>
    
    <!-- Modernizer Script for old Browsers -->		
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>

</head>
    
<body <?php body_class( ! is_home() && ! is_front_page() ? "is-page" : "" ); ?>>
    
    <div class="eupopup eupopup-top"></div>
        
    <header role="banner">
        <div class="bg-header">
            <svg class="wave-top" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 540"><path d="M-.8-1c.1 261 0 499.1-.5 499.8C349.2-36.3 985.1 190.4 1449.1-1H-.8z"/></svg>

            <div class="wave-top-scrolled"></div>
        </div>
        <div class="logo">
            <a href="<?php echo home_url(); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 566.9 453.5"><g id="camping-bungalows" fill="#7a858c"><path d="M66.5 427.6c1.4 0 2.6.3 3.8.8 1.2.5 2.2 1.2 3.2 2.1l-2.3 2.3c-1.3-1.3-2.8-1.9-4.7-1.9-.9 0-1.8.2-2.6.5s-1.5.8-2.1 1.4c-.6.6-1.1 1.3-1.4 2.1-.4.8-.5 1.7-.5 2.6s.2 1.8.5 2.6c.4.8.8 1.5 1.4 2.1.6.6 1.3 1.1 2.1 1.4.8.3 1.7.5 2.6.5 1.8 0 3.3-.6 4.7-1.9l2.3 2.3c-.9.9-2 1.6-3.2 2.1-1.2.5-2.5.8-3.8.8-1.4 0-2.7-.3-3.9-.8-1.2-.5-2.2-1.2-3.2-2.1-.9-.9-1.6-1.9-2.1-3.1-.5-1.2-.8-2.5-.8-3.9 0-1.3.3-2.6.8-3.8.5-1.2 1.2-2.3 2.1-3.2.9-.9 2-1.6 3.2-2.1 1.2-.5 2.5-.8 3.9-.8M97.3 444.1h-9.1l-1.7 3.3h-3.7l9.9-19.8 9.9 19.8H99l-1.7-3.3zm-7.5-3.2h5.8l-2.9-5.8-2.9 5.8zM115.4 435.6v11.8h-3.3v-19.8l9.9 9.9 9.9-9.9v19.8h-3.3v-11.8l-6.6 6.6zM144.6 440.9v6.6h-3.3v-19.8h9.9c1.8 0 3.4.6 4.7 1.9 1.3 1.3 1.9 2.9 1.9 4.7 0 1.8-.6 3.4-1.9 4.7-1.3 1.3-2.8 1.9-4.7 1.9h-6.6zm0-9.9v6.6h6.6c.9 0 1.7-.3 2.3-1 .6-.6 1-1.4 1-2.3 0-.9-.3-1.7-1-2.3-.6-.6-1.4-1-2.3-1h-6.6zM167.2 427.6h3.3v19.8h-3.3zM179.9 427.6l13.2 11.9v-11.9h3.3v19.8l-13.2-11.8v11.8h-3.3zM215.7 427.6c1.4 0 2.7.3 3.9.8 1.2.5 2.3 1.2 3.2 2.1l-2.4 2.3c-1.3-1.3-2.8-1.9-4.6-1.9-1.8 0-3.4.6-4.7 1.9-1.3 1.3-1.9 2.8-1.9 4.6s.6 3.4 1.9 4.7c1.3 1.3 2.9 1.9 4.7 1.9 1 0 1.9-.2 2.8-.6.8-.4 1.6-.9 2.2-1.6v-2.7h-5v-3.3h8.3v7.1c-.5.7-1 1.3-1.6 1.8-.6.6-1.3 1-2 1.4-.7.4-1.5.7-2.3.9-.8.2-1.6.3-2.5.3-2.7 0-5.1-1-7-2.9-1.9-1.9-2.9-4.3-2.9-7s1-5 2.9-7c2-1.8 4.3-2.8 7-2.8M258.8 447.4c-.6-.6-1.2-1.2-2-2.1-1.8 1.7-3.9 2.4-6.3 2.4-4.2 0-6.6-2.8-6.6-6 0-2.9 1.7-4.9 4.2-6.3v-.1c-1.1-1.4-1.8-3-1.8-4.5 0-2.6 1.9-5.3 5.4-5.3 2.7 0 4.7 1.8 4.7 4.5 0 2.3-1.3 4-4.7 5.7v.1c1.8 2.1 3.8 4.4 5.2 5.9 1-1.5 1.7-3.6 2.1-6.3h2.5c-.6 3.4-1.5 6-3.1 7.9 1.1 1.2 2.3 2.4 3.5 3.8h-3.1zm-3.4-3.6c-1.3-1.4-3.7-4-6-6.8-1.2.8-2.8 2.1-2.8 4.4 0 2.4 1.8 4.3 4.4 4.3 1.8 0 3.4-.8 4.4-1.9m-6.5-13.1c0 1.6.7 2.7 1.6 3.9 2.2-1.2 3.6-2.4 3.6-4.2 0-1.3-.7-2.8-2.5-2.8-1.8-.1-2.7 1.4-2.7 3.1M294.6 437.5c1.2 1.1 1.7 2.5 1.7 4.1 0 .8-.2 1.5-.5 2.2-.3.7-.7 1.3-1.2 1.8s-1.1.9-1.8 1.2c-.7.3-1.4.5-2.2.5h-10.7v-19.8h10.7c.8 0 1.5.2 2.2.5.7.3 1.3.7 1.9 1.2.5.5.9 1.1 1.2 1.8.3.7.5 1.4.5 2.2-.1 1.8-.7 3.2-1.8 4.3m-4.1-6.5h-7.4v5h7.4c.3 0 .6-.1.9-.2.3-.1.6-.3.8-.5.2-.2.4-.5.5-.8.1-.3.2-.6.2-1 0-.3-.1-.6-.2-.9-.1-.3-.3-.6-.5-.8-.2-.2-.5-.4-.8-.5-.2-.3-.5-.3-.9-.3m0 13.1c.3 0 .6-.1.9-.2.3-.1.6-.3.8-.5.2-.2.4-.5.5-.8.1-.3.2-.6.2-.9 0-.3-.1-.6-.2-.9-.1-.3-.3-.6-.5-.8-.2-.2-.5-.4-.8-.5-.3-.1-.6-.2-1-.2H283v4.9h7.5zM322.2 439.2c0 2.3-.8 4.2-2.4 5.8-1.6 1.6-3.6 2.4-5.8 2.4-2.2 0-4.2-.8-5.8-2.4-1.6-1.6-2.4-3.6-2.4-5.8v-11.6h3.3v11.6c0 1.4.5 2.5 1.4 3.5 1 1 2.1 1.5 3.5 1.5s2.5-.5 3.5-1.5 1.5-2.1 1.5-3.5v-11.6h3.3v11.6zM331.6 427.6l13.2 11.9v-11.9h3.3v19.8l-13.2-11.8v11.8h-3.3zM367.4 427.6c1.4 0 2.7.3 3.9.8 1.2.5 2.3 1.2 3.2 2.1l-2.4 2.3c-1.3-1.3-2.8-1.9-4.7-1.9-1.8 0-3.4.6-4.7 1.9-1.3 1.3-1.9 2.8-1.9 4.6s.6 3.4 1.9 4.7c1.3 1.3 2.9 1.9 4.7 1.9 1 0 1.9-.2 2.8-.6.8-.4 1.6-.9 2.2-1.6v-2.7h-5v-3.3h8.3v7.1c-.5.7-1 1.3-1.6 1.8-.6.6-1.3 1-2 1.4-.7.4-1.5.7-2.3.9-.8.2-1.6.3-2.5.3-2.7 0-5.1-1-7-2.9-1.9-1.9-2.9-4.3-2.9-7s1-5 2.9-7c2.1-1.8 4.4-2.8 7.1-2.8M398 444.1h-9.1l-1.7 3.3h-3.7l9.9-19.8 9.9 19.8h-3.7l-1.6-3.3zm-7.5-3.2h5.8l-2.9-5.8-2.9 5.8zM412.7 427.6h3.3v16.5h9.9v3.3h-13.2zM442.9 427.6c2.7 0 5.1 1 7 2.9 2 1.9 2.9 4.3 2.9 7 0 2.8-1 5.1-2.9 7-1.9 1.9-4.2 2.9-7 2.9-2.7 0-5.1-1-7-2.9-1.9-1.9-2.9-4.3-2.9-7s1-5.1 2.9-7c1.9-1.9 4.2-2.9 7-2.9m0 3.4c-1.8 0-3.4.6-4.7 1.9-1.3 1.3-1.9 2.8-1.9 4.7 0 1.8.6 3.4 1.9 4.7 1.3 1.3 2.9 1.9 4.7 1.9 1.8 0 3.4-.6 4.7-1.9 1.3-1.3 1.9-2.9 1.9-4.7 0-1.8-.6-3.4-1.9-4.7-1.4-1.3-2.9-1.9-4.7-1.9M462 427.6l6.3 12.5 6.2-12.5 6.2 12.5 6.2-12.5h3.7l-9.9 19.8-6.2-12.4-6.2 12.4-9.9-19.8zM503.8 439.2c-1.6 0-3-.6-4.1-1.7-1.1-1.1-1.7-2.5-1.7-4.1 0-1.6.6-2.9 1.7-4.1 1.1-1.1 2.5-1.7 4.1-1.7h10.7v3.3h-10.7c-.3 0-.7.1-1 .2-.3.1-.6.3-.8.5-.2.2-.4.5-.5.8-.1.3-.2.6-.2.9 0 .7.2 1.3.7 1.8.5.5 1.1.7 1.8.7h4.9c1.6 0 3 .6 4.1 1.7 1.1 1.1 1.7 2.5 1.7 4.1 0 1.6-.6 2.9-1.7 4.1-1.1 1.1-2.5 1.7-4.1 1.7H498v-3.3h10.7c.3 0 .7-.1 1-.2.3-.1.6-.3.8-.5.2-.2.4-.5.5-.8.1-.3.2-.6.2-1 0-.7-.2-1.2-.7-1.7-.5-.5-1.1-.7-1.7-.7h-5z"/></g><g id="la-gaviota" fill="#c67119"><path d="M5.1 351.8h8.4v42.3h25.3v8.4H5.1zM100 394.1H76.7l-4.3 8.5H63l25.4-50.7 25.4 50.7h-9.4l-4.4-8.5zm-19.2-8.4h14.9l-7.4-14.9-7.5 14.9zM194.8 351.8c3.5 0 6.8.7 9.9 2 3.1 1.3 5.8 3.1 8.1 5.5l-6.1 6c-3.3-3.3-7.2-5-11.9-5-4.6 0-8.6 1.7-12 5-3.3 3.3-4.9 7.3-4.9 11.9 0 4.6 1.6 8.6 4.9 12 3.4 3.3 7.4 4.9 12 4.9 2.5 0 4.9-.5 7-1.5 2.2-1 4-2.4 5.7-4.2v-6.9h-12.7V373H216v18.1c-1.2 1.7-2.5 3.3-4 4.7-1.5 1.4-3.2 2.6-5 3.6-1.8 1-3.7 1.8-5.8 2.3-2 .5-4.2.8-6.4.8-7 0-13-2.5-17.9-7.5-5-4.9-7.5-10.9-7.5-17.9 0-7 2.5-12.9 7.5-17.9 5-4.9 10.9-7.4 17.9-7.4M273 394.1h-23.3l-4.3 8.5H236l25.4-50.7 25.4 50.7h-9.4l-4.4-8.5zm-19.1-8.4h14.9l-7.4-14.9-7.5 14.9zM295.8 351.8l16 31.8 15.9-31.8h9.4l-25.3 50.7-25.4-50.7zM361.2 351.8h8.4v50.7h-8.4zM419.1 351.8c7 0 13 2.5 17.9 7.4 5 5 7.5 10.9 7.5 17.9 0 7.1-2.5 13-7.5 17.9-4.9 5-10.9 7.5-17.9 7.5-7 0-13-2.5-17.9-7.5-5-4.9-7.5-10.9-7.5-17.9 0-7 2.5-12.9 7.5-17.9 4.9-4.9 10.8-7.4 17.9-7.4m0 8.5c-4.6 0-8.6 1.7-12 5-3.3 3.3-4.9 7.3-4.9 11.9 0 4.6 1.6 8.6 4.9 12 3.4 3.3 7.3 4.9 12 4.9 4.6 0 8.6-1.6 11.9-4.9 3.3-3.4 5-7.4 5-12s-1.7-8.6-5-11.9c-3.3-3.3-7.3-5-11.9-5M461.7 351.8H504v8.5h-16.9v42.2h-8.4v-42.2h-17zM547.7 394.1h-23.3l-4.3 8.5h-9.4l25.4-50.7 25.4 50.7H552l-4.3-8.5zm-19.1-8.4h14.9l-7.5-14.9-7.4 14.9z"/></g><g id="logo"><path d="M262.8 95.1c.2-.9 18.8 83.7 74.9 133.5 19.1-3.6 41-3.7 59.7 2 .8.3 26.9 29.5 27.7 29.8C389.2 201.5 262.9 4.6 262.9 4.6L117.8 236.4l.4-.1.5-.5c2-1.6 4-3 6-4.3l.8-.4.3-.3c68-44 100.1 61.3 160.3-20.9-36.5 34.3-61.4 7.5-89.6-4.9 50.8-34.5 60.2-83.1 66.3-109.9" fill-rule="evenodd" clip-rule="evenodd" fill="#c67119"/><g fill-rule="evenodd" clip-rule="evenodd" fill="#3790bc"><path d="M302.3 297.9c78.4-76.9 97.3 29.4 158.3-22.5-55.4 73.3-83.8-24.5-158.3 22.5"/><path d="M115.3 284.3c46.1-37 85 16.2 128.1 12.3 27.1-2.5 50.5-35.5 82.3-48.8 44.3-18.5 82.2-2.6 104 23-17.5-45.7-65.1-62.2-111.6-43.3-28.9 11.7-51.1 44.1-78.9 44.9-40.7 1.1-81.5-46.7-123.9 11.9"/></g><g fill="#fff" stroke="#7a858c" stroke-width="2.026"><path d="M234.5 151.2C224 139.9 176 111.1 119.8 120 29 134.3 7.3 122.9 5.6 121.5c29.1 24.3 178.5 2.9 195.1 35 1.5 2.9-14.5-2.6-22.1 5-5.2 5.2-5.5 5.9-6.1 8.3-.7 2.4-.6 3.2-.8 3.5-.2.3-4.1 3.2-5.1 5.2-1 1.9-.7 2.7-.4 3.3l.4.6s0-.6.5-1.4c.5-.7 5.2-3.8 8.8-4.7 3.1-.8 8 1.5 17.8 3.4 20.4 4.2 58.4-2.6 74.2-6.6-16.5-.8-23.4-11.2-33.4-21.9z"/><path d="M210.5 155.1c8.9-18.8 33.3-22.4 50.1-24.7 9-1.2 58.9-3.1 74.9-3.4 38.3-.7 72.1-22.1 73.6-23.7-4.4 4.9-8.3 9-13.2 13.6-39.8 36.7-133.3 22.1-154.8 43.4"/></g></g></svg>
            </a>
        </div>
        
        <div class="header-right">
            <nav role="navigation" class="main-nav stretchy-nav">
                <a class="nav-trigger" href="#0"><span aria-hidden="true"></span></a>
                <?php html5blank_nav(); ?>
                
                <span aria-hidden="true" class="stretchy-nav-bg"></span>
            </nav>
            
            <div class="info">
                <nav class="lang-nav">
                    <!-- https://codepen.io/chamsi/pen/yKrvjd  -->
                    <?php qtranxf_generateLanguageSelectCode('text') ?>
                </nav>
                
                <a href="/plano-del-camping/" title="Mapa del cámping"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-map"></use></svg><span>Mapa del cámping</span></a>
                
                <a href="#" title="Webcam"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-videocam"></use></svg><span>Mapa del cámping</span></a>
                
                <a href="mailto:info@lagaviota.com" title="Contáctanos por email"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-email"></use></svg><span>Email</span></a>
                
                <a href="tel:0034972520569" title="Contáctanos por teléfono"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-telephone"></use></svg><span>+34 972 520 569</span></a>
            </div>
        </div>
        
    </header>
        

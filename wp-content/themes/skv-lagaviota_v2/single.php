<?php get_header(); ?>
        
        
    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="container single-accommodation" id="post-<?php the_ID(); ?>">
            <div class="grid">
                <div class="col-md-6 col-grid">
                    <div class="content-accom">

                        <div class="entry-meta">
                            <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                        </div>
                        
                        <div class="entry-content offer-content">
                            <ul class="items-list">
                                <?php if( get_field('item_1') ): ?>
                                <li><?php the_field('item_1'); ?></li>
                                <?php endif; ?>
                                <?php if( get_field('item_2') ): ?>
                                <li><?php the_field('item_2'); ?></li>
                                <?php endif; ?>
                                <?php if( get_field('item_3') ): ?>
                                <li><?php the_field('item_3'); ?></li>
                                <?php endif; ?>
                                <?php if( get_field('item_4') ): ?>
                                <li><?php the_field('item_4'); ?></li>
                                <?php endif; ?>
                                <?php if( get_field('item_5') ): ?>
                                <li><?php the_field('item_5'); ?></li>
                                <?php endif; ?>
                                <?php if( get_field('item_6') ): ?>
                                <li><?php the_field('item_6'); ?></li>
                                <?php endif; ?>
                            </ul>
                            
                            <?php the_content(); ?>
                            
                        </div>

                        <div class="share-icons">
                            <p>Comparte</p>
                            <ul class="social-media share">
                                <li>
                                    <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg>
                                        <span class="label">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="facebook" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg>
                                        <span class="label">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Comparte en Instagram" class="instagram" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg>
                                        <span class="label">Instagram</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                        <span class="label">WhatsApp</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.share-icons -->
                        
                        <div id="contactform" class="contact-form">
                            
                            <p class="fillform">Rellena el formulario para hacer tu reserva</p>
                            
                            <p><small>(*) Campos obligatorios</small></p>
                            
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                            <?php if (get_field('formulario_reserva_es')): ?>
                            <?php the_field('formulario_reserva_es'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                            <?php if (get_field('formulario_reserva_ca')): ?>
                            <?php the_field('formulario_reserva_ca'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                            <?php if (get_field('formulario_reserva_en')): ?>
                            <?php the_field('formulario_reserva_en'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='de'): ?>
                            <?php if (get_field('formulario_reserva_de')): ?>
                            <?php the_field('formulario_reserva_de'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='nl'): ?>
                            <?php if (get_field('formulario_reserva_nl')): ?>
                            <?php the_field('formulario_reserva_nl'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='fr'): ?>
                            <?php if (get_field('formulario_reserva_fr')): ?>
                            <?php the_field('formulario_reserva_fr'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php } ?>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6 col-grid">
                    <!-- post thumbnail -->
                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <?php the_post_thumbnail('large'); // Fullsize image for the single post ?>
                    <?php endif; ?>
                    <!-- /post thumbnail -->
                </div>
            </div>
        </section>
		<?php endwhile; ?>
		<?php else: ?>
        <section class="container container-padding" id="post-<?php the_ID(); ?>">
            <div class="copy entry-content">
                
                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                
            </div>
        </section>
		<?php endif; ?>
        
        
        <?php get_template_part( 'templates/content', 'related_offers' ); ?>
        
        
        <?php get_sidebar(); ?>
        
    </main>
        
        
<?php get_footer(); ?>

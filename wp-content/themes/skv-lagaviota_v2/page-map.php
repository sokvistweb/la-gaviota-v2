<?php /* Template Name: Page Map */ get_header('map'); ?>

    <main role="main" class="page page-map">
        
        <section class="container header-map">
            
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h1>Mapa del cámping</h1>
            <p>Clica en los puntos calientes para obtener más información</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h1>Mapa del càmping</h1>
            <p>Fes clic en els punts calents per obtenir més informació</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h1>Map of the campsite</h1>
            <p>Click on the hotspots for more information</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='de'): ?>
            <h1>Campingkarte</h1>
            <p>Klicken Sie auf die Hotspots, um weitere Informationen zu erhalten</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='nl'): ?>
            <h1>Camping kaart</h1>
            <p>Klik op de hotspots voor meer informatie</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='fr'): ?>
            <h1>Plan du camping</h1>
            <p>Cliquez sur les hotspots pour plus d'informations</p>
            <?php endif; ?>
            <?php } ?>
            
            <a class="back-home" href="<?php echo home_url(); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-lagaviota-return-home"></use></svg></a>
            
        </section>
                
        <div class="container-map">
            <div class="media-map">
                <img class="media-bg" src="<?php echo get_template_directory_uri(); ?>/assets/images/planol-lagaviota.jpg" alt="Camping La Gaviota map" width="1600" height="948">
                
                <div class="hotspots">
                    <a class="morph-btn hotspot xaloc" href="#xaloc"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-info"></use></svg></a>
                    
                    <a class="morph-btn hotspot llevant" href="#llevant"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-info"></use></svg></a>
                    
                    <a class="morph-btn hotspot tamariu" href="#tamariu"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-info"></use></svg></a>
                    
                    <a class="morph-btn hotspot morea" href="#morea"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-info"></use></svg></a>
                    
                    <a class="morph-btn hotspot piscina" href="#piscina"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-swimming-pool"></use></svg></a>
                    
                    <a class="morph-btn hotspot playground" href="#playground"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-playground"></use></svg></a>
                    
                    <a class="morph-btn hotspot playground2" href="#playground2"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-playground"></use></svg></a>
                    
                    <a class="morph-btn hotspot gastronomia" href="#gastronomia"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-restaurant"></use></svg></a>
                    
                    <a class="morph-btn hotspot sanitarios" href="#sanitarios"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-toilet"></use></svg></a>
                    
                    <a class="morph-btn hotspot lavanderia" href="#lavanderia"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-washing-machine"></use></svg></a>
                    
                    <a class="morph-btn hotspot supermarket" href="#supermarket"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-supermarket"></use></svg></a>
                    
                    <a class="morph-btn hotspot reception" href="#reception"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-reception"></use></svg></a>
                    
                    <a class="morph-btn hotspot sailwash" href="#sailwash"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-sail-wash"></use></svg></a>
                    
                    <a class="morph-btn hotspot playfield" href="#playfield"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-playfield"></use></svg></a>
                    
                    <a class="morph-btn hotspot windsurf" href="#windsurf"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-windsurf"></use></svg></a>
                    
                    <a class="morph-btn hotspot sunbath" href="#sunbath"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-sunbath"></use></svg></a>
                    
                    <a class="morph-btn hotspot beachbar" href="#beachbar"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-vermout"></use></svg></a>
                    
                    <a class="morph-btn hotspot entertainments" href="#entertainments"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-miniclub"></use></svg></a>
                </div>
                
            </div>
        </div>
            
        <div class="hotspots-content">
            <div class="hotspot-content morph-modal" id="xaloc">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'xaloc' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="llevant">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'llevant' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="tamariu">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'tamariu' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="morea">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'morea' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="piscina">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'piscina' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="playground">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'playground' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="playground2">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'playground' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="gastronomia">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'gastronomia' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="sanitarios">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'sanitarios' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="lavanderia">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'lavanderia' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="supermarket">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'supermarket' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="reception">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'reception' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="sailwash">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'sailwash' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="playfield">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'playfield' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="windsurf">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'windsurf' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="sunbath">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'sunbath' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="beachbar">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'beachbar' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="entertainments">
                <div class="content-map modal-content">
                    <?php get_template_part( 'templates-map/content', 'entertainments' ); ?>
                </div>
                <button class="close-modal"></button><span class="morph-background"></span>
            </div>
        </div>
            
    </main>


    <main role="main" class="page page-map-mobile">
        
        <section class="container header-map">
            
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h1>Mapa del cámping</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h1>Mapa del càmping</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h1>Map of the campsite</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='de'): ?>
            <h1>Campingkarte</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='nl'): ?>
            <h1>Camping kaart</h1>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='fr'): ?>
            <h1>Plan du camping</h1>
            <?php endif; ?>
            <?php } ?>
            
            <a class="back-home" href="<?php echo home_url(); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-lagaviota-return-home"></use></svg></a>
            
        </section>
                
        <div class="container-map">
            <div class="media-map">
                <img class="media-bg" src="<?php echo get_template_directory_uri(); ?>/assets/images/planol-lagaviota-mobile.jpg" alt="Camping La Gaviota map" width="600" height="810">
            </div>
        </div>
            
    </main>


<?php get_footer('map'); ?>

<?php /* Template Name: Page Accomodation */ get_header(); ?>

    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,6)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        <section class="container">
            
            <ul class="features grid align-center">
                <?php query_posts('post_type=page&name=mobilhome-llevant'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=mobilhome-xaloc'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=mobilhome-tamariu'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=bungalow-morea'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=parcelas'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <figure>
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('medium');
                        endif; ?>
                        <figcaption>
                            <h2>
                                <?php
                                $words    = explode( ' ', the_title( '', '',  false ) );
                                $words[0] = '<span>' . $words[0] . '</span>';
                                $title    = implode( ' ', $words );
                                echo $title;    
                                ?>
                            </h2>
                            <ul class="actions">
                                <li><span class="min-price"><?php the_field('precio_desde'); ?></span></li>
                                <li><a href="<?php the_permalink(); ?>" class="button more-info" title="<?php the_title() ?>"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li>
                                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                    <?php if (qtranxf_getLanguage()=='es'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=es" class="button" title="Página de reservas" target="_blank">Reservar<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=ca" class="button" title="Pàgina de reserves" target="_blank">Reservar<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='en'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=en" class="button" title="Booking page" target="_blank">Book now<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='de'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=de" class="button" title="Booking page" target="_blank">Buche Jetzt<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='nl'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=nl" class="button" title="Booking page" target="_blank">Boek Nu<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='fr'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=fr" class="button" title="Réserver" target="_blank">Réserver<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>
                                    <?php } ?>
                                </li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=plano-del-camping'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4 is-map">
                    <figure>
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('medium');
                        endif; ?>
                        <figcaption>
                            <h2>
                                <?php
                                $words    = explode( ' ', the_title( '', '',  false ) );
                                $words[0] = '<span>' . $words[0] . '</span>';
                                $title    = implode( ' ', $words );
                                echo $title;    
                                ?>
                            </h2>
                            <a href="<?php the_permalink(); ?>" class="" title="<?php the_title() ?>"><span>Més info</span></a>
                        </figcaption>			
                    </figure>
                </li>
                <?php endwhile; wp_reset_query(); ?>
            </ul>
            
        </section>
		
            
        <?php get_template_part( 'templates/content', 'reviews' ); ?>
        
        
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

<?php get_header(); ?>
        
        
    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="container single-accommodation" id="post-<?php the_ID(); ?>">
            <div class="grid">
                <div class="col-md-6 col-grid">
                    <div class="content-inspire">
                        <div class="entry-content offer-content">
                            
                            <?php the_content(); ?>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-grid">
                    <ul id="accommodation-gallery" class="accom-gallery">
                        <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <li data-thumb="<?php the_post_thumbnail_url('thumbnail'); ?>"><?php the_post_thumbnail('large'); ?></li>
                        <?php endif; ?>
                        <!-- ACF -->
                        <?php $image = get_field('img_slider_1');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_2');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_3');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_4');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_5');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_6');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </section>
		<?php endwhile; ?>
		<?php else: ?>
        <section class="container container-padding" id="post-<?php the_ID(); ?>">
            <div class="copy entry-content">
                
                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                
            </div>
        </section>
		<?php endif; ?>
        
        
        <?php get_template_part( 'templates/content', 'related_inspirate' ); ?>
        
        
        <?php get_sidebar(); ?>
        
    </main>
        
        
<?php get_footer(); ?>

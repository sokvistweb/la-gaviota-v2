<?php 
    /* Template Name: Tamariu
       Displays Tamariu page content */
?>

<?php query_posts('post_type=page&name=bungalow-morea'); while (have_posts ()): the_post(); ?>
<section class="container heading">
    <h2><?php the_title(); ?></h2>
</section>

<section class="container single-accommodation" id="post-<?php the_ID(); ?>">
    <div class="grid">
        <div class="col-md-7 col-grid">
            <ul id="gallery-morea" class="accom-gallery">
                <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                <li data-thumb="<?php the_post_thumbnail_url('thumbnail'); ?>"><?php the_post_thumbnail('large'); ?></li>
                <?php endif; ?>
                <!-- ACF -->
                <?php $image = get_field('imagen_slider_1');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_2');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_3');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_4');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_5');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_6');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="col-md-5 col-grid">
            <div class="content-accom entry-content">

                <?php the_content(); ?>

                <?php if( get_field('link_inventario') || get_field('texto_inventario') ): ?>
                <p><a href="<?php the_field('link_inventario'); ?>" class="household" target="_blank"><span><?php the_field('texto_inventario'); ?></span><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-alt"></use></svg></a></p>
                <?php endif; ?>

                <?php if( get_field('link_reservar') || get_field('texto_reservar') ): ?>
                <a href="<?php the_field('link_reservar'); ?>" class="button btn-icon" target="_blank"><?php the_field('texto_reservar'); ?><svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>

                <?php if( get_field('dudas') ): ?>
                <p><?php the_field('dudas'); ?></p>
                <?php endif; ?>

                <div class="share-icons">
                    <p>Comparte</p>
                    <ul class="social-media share">
                        <li>
                            <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg>
                                <span class="label">Twitter</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="facebook" target="_blank">
                                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg>
                                <span class="label">Facebook</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Comparte en Instagram" class="instagram" target="_blank">
                                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg>
                                <span class="label">Instagram</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                <span class="label">WhatsApp</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- /.share-icons -->
            </div>
        </div>
    </div>
</section>
<?php endwhile; wp_reset_query(); ?>

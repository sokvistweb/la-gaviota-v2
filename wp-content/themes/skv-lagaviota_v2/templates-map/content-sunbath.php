<?php 
    /* Template Name: Sunbath
       Displays Sunbath page content */
?>

<?php query_posts('post_type=hotspots&name=zona-de-playa'); while (have_posts ()): the_post(); ?>
<section class="container heading">
    <h2><?php the_title(); ?></h2>
</section>

<section class="container cards">
    <div class="grid">
        <div class="col-md-7 col-grid">
            <ul id="gallery-playground">
                <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                <li data-thumb="<?php the_post_thumbnail_url('thumbnail'); ?>"><?php the_post_thumbnail('large'); ?></li>
                <?php endif; ?>
                <!-- ACF -->
                <?php $image = get_field('imagen_slider_01');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_02');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_03');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <?php $image = get_field('imagen_slider_04');
                if( !empty($image) ): ?>
                <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                <?php endif; ?>
                <!-- /ACF -->
            </ul>
        </div>
        <div class="col-md-5 col-grid">
            <div class="content-accom entry-content">
                <?php the_content(); ?>
            </div>
        </div>

    </div>
</section>
<?php endwhile; wp_reset_query(); ?>

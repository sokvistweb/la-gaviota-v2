<?php 
    /* Template Name: Gastronomia
       Displays Gastronomia page content */
?>



<?php query_posts(array( 'post_type' => 'page', 'post_parent' => '156', 'orderby' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => 10 )); while (have_posts ()): the_post(); ?>

<section class="container h-cards">
    <div class="grid">
        
        <div class="col-sm-12 col-md-12 col-lg-12 col-grid direction-row">
                    <div class="col-xs-4 col-12">
                    <!-- post thumbnail -->
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium', array('class' => 'flex-img')); // Declare pixel size you need inside the array ?>
                    <?php else: // field_name returned false ?>
                        <img class="flex-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/no-thumbnail.jpg">
                    <?php endif; ?>
                    <!-- /post thumbnail -->
                    </div>
                    <div class="col-xs-8 col-12">
                        <div class="h-card-body">
                            <h2><?php the_title() ?></h2>
                            <?php the_excerpt() ?>
                        </div>
                    </div>
                </div>

    </div>
</section>
<?php endwhile; wp_reset_query(); ?>

<?php get_header(); ?>

    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php single_post_title(); ?></h1>
        </section>
        
        
        <section class="container offer-cards">
            <div class="grid">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <div class="card-body">
                        <a href="single-offer.php">
                            <!-- post thumbnail -->
                            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php the_post_thumbnail('medium'); // Declare pixel size you need inside the array ?>
                                </a>
                            <?php endif; ?>
                            <!-- /post thumbnail -->
                        </a>
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                        <div class="entry-meta">
                            <span class="post-date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                        </div>
                        <div class="entry-content clearfix">
                            <?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
                        </div>
                    </div>
                    <div class="flex-footer">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button">Reservar</a>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
        
        <section class="container">
            
            <div class="pagination">
                <?php wp_numeric_posts_nav(); ?>
            </div>
            
        </section>
        
        
        <?php get_template_part( 'templates/content', 'reviews' ); ?>
        
        
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

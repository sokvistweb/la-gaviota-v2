<?php if(function_exists('qtranxf_getLanguage')) { ?>
<!--  SPANISH   ///////////////////////// -->
<?php if (qtranxf_getLanguage()=='es'): ?>


<section class="container pre-footer">
    <div class="grid">
        <div class="col-md-6 col-bleed">
            <h3>Suscríbete</h3>
            <div class="mailchimp-wrapper">
                <p>Infórmate de nuestras ofertas y novedades</p>
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                            <div class="p-accept">
                            <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="/politica-de-privacidad">política de privacidad</a></span>
                            </div>
                            <div class="clear">
                                <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Ends MailChimp Signup Form -->
            </div>
        </div>
        <div class="col-md-6 col-bleed">
            <h3>Cómo llegar</h3>
            <a href="https://g.page/campinglagaviota?share" class="map" title="Ver el mapa de Google" target="_blank"><span>Ver en Google Maps</span></a>
        </div>
    </div>
</section>


<?php endif; ?>
<!--  CATALAN   ///////////////////////// -->
<?php if (qtranxf_getLanguage()=='ca'): ?>


<section class="container pre-footer">
    <div class="grid">
        <div class="col-md-6 col-bleed">
            <h3>Subscriu-te</h3>
            <div class="mailchimp-wrapper">
                <p>Informa't de les nostres ofertes i novetats</p>
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                            <div class="p-accept">
                            <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He llegit i accepto la vostra <a href="/ca/politica-de-privacidad">política de privacitat</a></span>
                            </div>
                            <div class="clear">
                                <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Ends MailChimp Signup Form -->
            </div>
        </div>
        <div class="col-md-6 col-bleed">
            <h3>Com arribar</h3>
            <a href="https://g.page/campinglagaviota?share" class="map" title="Veure el mapa a Google" target="_blank"><span>Veure a Google Maps</span></a>
        </div>
    </div>
</section>


<?php endif; ?>
<!--  ENGLISH   ///////////////////////// -->
<?php if (qtranxf_getLanguage()=='en'): ?>


<section class="container pre-footer">
    <div class="grid">
        <div class="col-md-6 col-bleed">
            <h3>Subscribe</h3>
            <div class="mailchimp-wrapper">
                <p>Find out about our offers and news</p>
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=5b7bc534af79a971e561b5b6e&amp;id=ebe838fe9c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                            <div class="p-accept">
                            <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>I have read and accept your <a href="/en/politica-de-privacidad">privacy policy</a></span>
                            </div>
                            <div class="clear">
                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Ends MailChimp Signup Form -->
            </div>
        </div>
        <div class="col-md-6 col-bleed">
            <h3>Getting there</h3>
            <a href="https://g.page/campinglagaviota?share" class="map" title="View map on Google" target="_blank"><span>View on Google Maps</span></a>
        </div>
    </div>
</section>


<?php endif; ?>
<!--  GERMAN   ///////////////////////// -->
<?php if (qtranxf_getLanguage()=='de'): ?>


<section class="container pre-footer">
    <div class="grid">
        <div class="col-md-6 col-bleed">
            <h3>Registrieren</h3>
            <div class="mailchimp-wrapper">
                <p>Informieren Sie sich über unsere Angebote und Neuigkeiten</p>
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=ab54c7b28cd2047845f9a89ab&amp;id=8679a79e69" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                            <div class="p-accept">
                            <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>Ich habe Ihre <a href="/de/politica-de-privacidad">Datenschutzerklärung</a> gelesen und akzeptiere sie.</span>
                            </div>
                            <div class="clear">
                                <input type="submit" value="Schicken" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Ends MailChimp Signup Form -->
            </div>
        </div>
        <div class="col-md-6 col-bleed">
            <h3>Anfahrtsweg</h3>
            <a href="https://g.page/campinglagaviota?share" class="map" title="Auf Google Maps anzeigen" target="_blank"><span>Auf Google Maps anzeigen</span></a>
        </div>
    </div>
</section>


<?php endif; ?>
<!--  DUTCH   ///////////////////////// -->
<?php if (qtranxf_getLanguage()=='nl'): ?>


<section class="container pre-footer">
    <div class="grid">
        <div class="col-md-6 col-bleed">
            <h3>Schrijf je in</h3>
            <div class="mailchimp-wrapper">
                <p>Ontdek onze aanbiedingen en nieuws</p>
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=5b7bc534af79a971e561b5b6e&amp;id=ebe838fe9c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                            <div class="p-accept">
                            <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="/nl/politica-de-privacidad">política de privacidad</a></span>
                            </div>
                            <div class="clear">
                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Ends MailChimp Signup Form -->
            </div>
        </div>
        <div class="col-md-6 col-bleed">
            <h3>Route beschrijving</h3>
            <a href="https://g.page/campinglagaviota?share" class="map" title="Bekijk op Google Maps" target="_blank"><span>Bekijk op Google Maps</span></a>
        </div>
    </div>
</section>


<?php endif; ?>
<!--  FRENCH   ///////////////////////// -->
<?php if (qtranxf_getLanguage()=='fr'): ?>


<section class="container pre-footer">
    <div class="grid">
        <div class="col-md-6 col-bleed">
            <h3>S'abonner</h3>
            <div class="mailchimp-wrapper">
                <p>Découvrez nos offres et nouveautés</p>
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=b89ef3edde" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                            <div class="p-accept">
                            <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>J'ai lu et j'accepte votre <a href="/fr/politica-de-privacidad">politique de confidentialité</a></span>
                            </div>
                            <div class="clear">
                                <input type="submit" value="Envoyer" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Ends MailChimp Signup Form -->
            </div>
        </div>
        <div class="col-md-6 col-bleed">
            <h3>Comment arriver</h3>
            <a href="https://g.page/campinglagaviota?share" class="map" title="Afficher sur Google Maps" target="_blank"><span>Afficher sur Google Maps</span></a>
        </div>
    </div>
</section>


<?php endif; ?>
<?php } ?>

  var lang;
  var FrameWidge=null;
  var URLBASEJS='https://bookings.masterasp.com/js/'
  var ESTAB;
  var clientCookies=false;
  
  function addit() {

    if (typeof(masp_calendar_is_loaded) != "undefined" && masp_calendar_is_loaded &&
    typeof(masp_widget_is_loaded) != "undefined" &&
    masp_widget_is_loaded) {
	
		if (enabledCookies() && clientCookies )	{
			FrameWidget = load_widgetCookies('bookings-widget', 'bookings-widget-calendar', ESTAB, lang,true);	  
		} else {
			FrameWidget = load_widget('bookings-widget', 'bookings-widget-calendar', ESTAB, lang);
		}
    }
    else {
      setTimeout('addit()', 1000);
    }

  }

 

  function add_widget(l,EstabID){
    ESTAB=EstabID;
    if (l=='') l='ES';
    lang = l
    datetime=new Date();

    var h = document.getElementsByTagName('HEAD')[0];

    var a = new Array('AnchorPosition.js?a='+datetime.valueOf(), 'calendar2.js?a='+datetime.valueOf(),'widgethtml.js?a'+datetime.valueOf());

    for (var i = 0; i < a.length; i++) {
      var s = document.createElement('script');
      s.src = URLBASEJS + a[i];
      h.appendChild(s);
    }
    addit(EstabID);

  }  
<?php 
    /* Template Name: Booking
       Displays Booking Engine form */
?>

<div class="container-widget-booking">
    <div class="booking-popup">
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='es'): ?>
        <h2><a href="https://booking.lagaviota.com/search" target="_blank" class="" rel="" title="Ir a la página de reservas">Haz tu reserva<svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-booking"></use></svg></a></h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <h2><a href="https://booking.lagaviota.com/search" target="_blank" class="" rel="" title="Aneu a la pàgina de reserves">Reservar<svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-booking"></use></svg></a></h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        <h2><a href="https://booking.lagaviota.com/search" target="_blank" class="" rel="" title="Go to booking page">Book now<svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-booking"></use></svg></a></h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='de'): ?>
        <h2><a href="https://booking.lagaviota.com/search" target="_blank" class="" rel="" title="Buche Jetzt">Buche Jetzt<svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-booking"></use></svg></a></h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='nl'): ?>
        <h2><a href="https://booking.lagaviota.com/search" target="_blank" class="" rel="" title="Boek Nu">Boek Nu<svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-booking"></use></svg></a></h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='fr'): ?>
        <h2><a href="https://booking.lagaviota.com/search" target="_blank" class="" rel="" title="Réserver">Réserver<svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-booking"></use></svg></a></h2>
        <?php endif; ?>
        <?php } ?>
    </div>
    
    
    <div class="container-booking" id="booking-modal">
        
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='es'): ?>
        <h2>Haz tu reserva</h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <h2>Reservar</h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        <h2>Book now</h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='de'): ?>
        <h2>Buche Jetzt</h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='nl'): ?>
        <h2>Boek Nu</h2>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='fr'): ?>
        <h2>Réserver</h2>
        <?php endif; ?>
        <?php } ?>
        <!-- Booking engine -->
        <div id="widgetBookingContainer-wrapper">
            <div id="widgetBookingContainer" class="widget_columns"></div>
        </div>
        
        
        <!-- <div id="widgetBookingContainer-wrapper">
            <div id="widgetBookingContainer" class="widget_columns widgetBookingContainer">
                <div id="c7e2df699b4440badc961ab0d73e5276" class="categorySelector ">
                    <span id="c7e2df699b4440badc961ab0d73e5276_input" class="inputBox empty enabled" onclick="coman.eventHandles.h_c7e2df699b4440badc961ab0d73e5276_5ac32504(event, undefined)">Cualquier alojamiento&nbsp;</span>
                </div>
                <div id="4a229fadc36e896af71eb425bdd59bcb" class="checkinCheckoutContainer">
                    <div class="checkinContainer">
                        <label for="4a229fadc36e896af71eb425bdd59bcb_checkin" id="74cf746de90caa03d380f55a519d6d39" class="checkinLabel label">Fecha de entrada:</label>
                        <div id="f0d0ceb34fa9298b52e1fdc8925f2cb8" class="datePicker "><span id="f0d0ceb34fa9298b52e1fdc8925f2cb8_input" class="inputBox empty enabled invalid" onclick="coman.eventHandles.h_f0d0ceb34fa9298b52e1fdc8925f2cb8_255452fc(event, undefined)">Fecha de entrada</span>
                        </div>
                    </div>
                    <div class="checkoutContainer">
                        <label for="4a229fadc36e896af71eb425bdd59bcb_checkout" id="e4bc2735efb9ffdb35f1322b77651480" class="checkoutLabel label">Fecha de salida:</label>
                        <div id="459e3fe6f6a17c2ddd83b4adaacef031" class="datePicker checkout"><span id="459e3fe6f6a17c2ddd83b4adaacef031_input" class="inputBox empty dissabled invalid">Fecha de salida</span>
                        </div>
                    </div>
                </div>
                <div class="peopleContainer">
                    <div class="adultsContainer">
                        <label for="3ee03d98c3ad4e4d7ff3e531a7cbda30_adults" id="d2f21ab5b6046cf39d9c50da78d11870" class="label">Adultos:</label>
                        <div id="e8a2454724b7918558345d64eb648148" class="comboBox " onclick="coman.eventHandles.h_e8a2454724b7918558345d64eb648148_5ac32504(event, undefined)"><span id="e8a2454724b7918558345d64eb648148_input" class="inputBox enabled">2 adultos&nbsp;</span>
                        </div>
                    </div>
                    <div class="childsGroupContainer">
                        <div class="childsContainer">
                        <label for="3ee03d98c3ad4e4d7ff3e531a7cbda30_childs" id="69c35503a78c081dc3b1caf2701f1ff7" class="label">Niños:</label>
                        <div id="e85d9e34bdbb7363bbbe47b8b217a274" class="comboBox " onclick="coman.eventHandles.h_e85d9e34bdbb7363bbbe47b8b217a274_5ac32504(event, undefined)"><span id="e85d9e34bdbb7363bbbe47b8b217a274_input" class="inputBox enabled">0 niños&nbsp;</span>
                        </div>
                        </div>
                        <div class="childAgeGroupContainer"></div>
                    </div>
                </div>
                <div id="8b56afdb99b308006e33765f02327497" class="facilities ">
                    <div class="facilitiesTitle">Servicios</div>
                    <div id="8b56afdb99b308006e33765f02327497_facility_1" class="facilityContainer">
                        <div id="8b56afdb99b308006e33765f02327497_facilityCB_1" class="facilityGroupCB unselected" onclick="coman.eventHandles.h_8b56afdb99b308006e33765f02327497_1219d3dc(event, 1)"><span>unselected</span>
                        </div>
                        <label id="8b56afdb99b308006e33765f02327497_categoryLabel_1" class="facilityLabel" onclick="coman.eventHandles.h_8b56afdb99b308006e33765f02327497_1219d3dc(event, 1)">Perros Admitidos</label>
                    </div>
                    <div id="8b56afdb99b308006e33765f02327497_facility_3" class="facilityContainer">
                        <div id="8b56afdb99b308006e33765f02327497_facilityCB_3" class="facilityGroupCB unselected" onclick="coman.eventHandles.h_8b56afdb99b308006e33765f02327497_1219d3dc(event, 3)"><span>unselected</span>
                        </div>
                        <label id="8b56afdb99b308006e33765f02327497_categoryLabel_3" class="facilityLabel" onclick="coman.eventHandles.h_8b56afdb99b308006e33765f02327497_1219d3dc(event, 3)">Adaptado minusvalidos</label>
                    </div>
                </div>

                <button class="searchButton button disabled" id="a85e38af455ae29b4d8ccdf8935567f6" onclick="coman.eventHandles.h_a85e38af455ae29b4d8ccdf8935567f6_40b1cb46(event, undefined)" disabled="">Buscar</button>
            </div>
        </div> -->
        
        
    </div>
</div>
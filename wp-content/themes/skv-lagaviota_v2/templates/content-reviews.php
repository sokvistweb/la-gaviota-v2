<?php 
    /* Template Name: Reviews
       Displays Reviews slider */
?>

<section class="container">
    <div class="reviews">
        <div class="grid">
            <div class="col-12">
                <ul id="reviews-slider">
                    <?php query_posts(array( 'post_type' => 'reviews' , 'posts_per_page' => 8 )); ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <li>
                        <blockquote><?php the_content(); ?>
                            <span><?php the_title(); ?></span>
                        </blockquote>
                    </li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

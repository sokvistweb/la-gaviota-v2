<?php 
    /* Template Name: Related Parcelas
       Displays related pages on Parcelas page */
?>

<section class="container more-accom">
    
    <h2>También te puede interesar</h2>
    
    <ul class="features grid align-center">
        <?php query_posts('post_type=page&name=mobilhome-llevant'); while (have_posts ()): the_post(); ?>
        <li class="icon col-grid col-md-3">
            <?php get_template_part( 'templates/content', 'related_grid' ); ?>
        </li>
        <?php endwhile; wp_reset_query(); ?>
        <?php query_posts('post_type=page&name=mobilhome-xaloc'); while (have_posts ()): the_post(); ?>
        <li class="icon col-grid col-md-3">
            <?php get_template_part( 'templates/content', 'related_grid' ); ?>
        </li>
        <?php endwhile; wp_reset_query(); ?>
        <?php query_posts('post_type=page&name=mobilhome-tamariu'); while (have_posts ()): the_post(); ?>
        <li class="icon col-grid col-md-3">
            <?php get_template_part( 'templates/content', 'related_grid' ); ?>
        </li>
        <?php endwhile; wp_reset_query(); ?>
        <?php query_posts('post_type=page&name=bungalow-morea'); while (have_posts ()): the_post(); ?>
        <li class="icon col-grid col-md-3">
            <?php get_template_part( 'templates/content', 'related_grid' ); ?>
        </li>
        <?php endwhile; wp_reset_query(); ?>
    </ul>

</section>
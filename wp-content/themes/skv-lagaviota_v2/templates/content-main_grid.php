<?php 
    /* Template Name: Main Grid
       Displays Home page grid */
?>

<figure>
    <?php if ( has_post_thumbnail()):
        the_post_thumbnail('medium');
    endif; ?>
    <figcaption>
        <h2>
            <?php
            $words    = explode( ' ', the_title( '', '',  false ) );
            $words[1] = '<span>' . $words[1] . '</span>';
            $title    = implode( ' ', $words );
            echo $title;    
            ?>
        </h2>
        <ul class="actions">
            <li><span class="min-price"><?php the_field('precio_desde'); ?></span></li>
            <li><a href="<?php the_permalink(); ?>" class="button more-info" title="<?php the_title() ?>"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
            <li>
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                <a href="https://booking.lagaviota.com/search?lang=es" class="button" title="Página de reservas" target="_blank">Reservar<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                <a href="https://booking.lagaviota.com/search?lang=ca" class="button" title="Pàgina de reserves" target="_blank">Reservar<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                <a href="https://booking.lagaviota.com/search?lang=en" class="button" title="Booking page" target="_blank">Book now<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='de'): ?>
                <a href="https://booking.lagaviota.com/search?lang=de" class="button" title="Booking page" target="_blank">Buche Jetzt<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='nl'): ?>
                <a href="https://booking.lagaviota.com/search?lang=nl" class="button" title="Booking page" target="_blank">Boek Nu<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                <a href="https://booking.lagaviota.com/search?lang=fr" class="button" title="Réserver" target="_blank">Réserver<svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                <?php endif; ?>
                <?php } ?>
            </li>
        </ul>
    </figcaption>
</figure>
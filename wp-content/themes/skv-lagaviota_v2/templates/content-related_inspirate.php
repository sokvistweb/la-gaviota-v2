<?php 
    /* Template Name: Related Offers
       Displays related offers on single offer */
?>

<section class="container offer-cards single-offer single-inspire">
    
    <h2>Inspírate</h2>
    
    <div class="grid">
    <?php
    $related = get_posts( 
        array( 
            'category__in' => wp_get_post_categories($post->ID), 
            'numberposts' => 3,
            'post_type' => 'inspiraciones',
            'post__not_in' => array($post->ID) 
        ) 
    );
    if( $related ) foreach( $related as $post ) {
    setup_postdata($post); ?>
    
        <div class="col-md-4 col-grid">
            <div class="card-body">
                <!-- post thumbnail -->
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_post_thumbnail('medium'); // Declare pixel size you need inside the array ?>
                    </a>
                <?php endif; ?>
                <!-- /post thumbnail -->
                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                <div class="entry-content clearfix">
                    <?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
                </div>
            </div>
            <div class="flex-footer">
                <a href="<?php the_permalink(); ?>" class="button is-blue more-info" title="<?php the_title() ?>"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a>
            </div>
        </div>
    <?php }
    wp_reset_postdata(); ?>
        
    </div>


</section>
<?php get_header(); ?>


    <main role="main">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,6)); ?>">
            
            <!--<picture class="hero-image">
            <?php //jpg images
            $image = get_field('hero_image_1');
            $url = $image['url'];
            $alt = $image['alt']; ?>

            <?php if( $image ): // Tablet
                $size = 'hero_image_tablet';
                $thumb = $image['sizes'][ $size ];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ]; ?>

                <source media="(max-width: 1024px)" srcset="<?php echo esc_url($thumb); ?>"  sizes="(min-width: 1024px)" loading="lazy" />

            <?php endif; ?>
            <?php if( $image ): // Desktop
                $size = 'hero_image';
                $thumb = $image['sizes'][ $size ];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ]; ?>

                <img class="" src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" loading="lazy" />

            <?php endif; ?>
            </picture>-->
            
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
            
            <div class="weather container">
                <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-front-page')) ?>
            </div>
        </section>
        
        
        <section class="container">
            
            <ul class="features grid align-center">
                <?php query_posts('post_type=page&name=mobilhome-llevant'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=mobilhome-xaloc'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=mobilhome-tamariu'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=bungalow-morea'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <?php get_template_part( 'templates/content', 'main_grid' ); ?>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=parcelas'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4">
                    <figure>
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('medium');
                        endif; ?>
                        <figcaption>
                            <h2>
                                <?php
                                $words    = explode( ' ', the_title( '', '',  false ) );
                                $words[0] = '<span>' . $words[0] . '</span>';
                                $title    = implode( ' ', $words );
                                echo $title;    
                                ?>
                            </h2>
                            <ul class="actions">
                                <li><span class="min-price"><?php the_field('precio_desde'); ?></span></li>
                                <li><a href="<?php the_permalink(); ?>" class="button more-info" title="<?php the_title() ?>"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li>
                                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                    <?php if (qtranxf_getLanguage()=='es'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=es" class="button" title="Página de reservas" target="_blank">Reservar</a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=ca" class="button" title="Pàgina de reserves" target="_blank">Reservar</a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='en'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=en" class="button" title="Booking page" target="_blank">Book now</a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='de'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=de" class="button" title="Booking page" target="_blank">Buche Jetzt</a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='nl'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=nl" class="button" title="Booking page" target="_blank">Boek Nu</a>
                                    <?php endif; ?>
                                    <?php if (qtranxf_getLanguage()=='fr'): ?>
                                    <a href="https://booking.lagaviota.com/search?lang=fr" class="button" title="Réserver" target="_blank">Réserver</a>
                                    <?php endif; ?>
                                    <?php } ?>
                                </li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                <?php query_posts('post_type=page&name=plano-del-camping'); while (have_posts ()): the_post(); ?>
                <li class="icon col-grid col-md-4 is-map">
                    <figure>
                        <?php if ( has_post_thumbnail()):
                            the_post_thumbnail('medium');
                        endif; ?>
                        <figcaption>
                            <h2>
                                <?php
                                $words    = explode( ' ', the_title( '', '',  false ) );
                                $words[0] = '<span>' . $words[0] . '</span>';
                                $title    = implode( ' ', $words );
                                echo $title;    
                                ?>
                            </h2>
                            <a href="<?php the_permalink(); ?>" class="" title="<?php the_title() ?>"><span>Més info</span></a>
                        </figcaption>			
                    </figure>
                </li>
                <?php endwhile; wp_reset_query(); ?>
                
            <?php if ( have_rows( 'boxes_home' ) ) : ?>
            <?php while ( have_rows( 'boxes_home' ) ) : the_row(); ?>
                <?php if ( have_rows( 'box_1' ) ) : ?>
                <?php while ( have_rows( 'box_1' ) ) : the_row(); ?>
                <li class="icon col-grid col-md-4">
                    <figure>
                    <?php
                    $imagen = get_sub_field('imagen');
                    if( $imagen ):
                        // Image variables.
                        $url = $imagen['url'];
                        $title = $imagen['title'];
                        $alt = $imagen['alt'];
                        // Thumbnail size attributes.
                        $size = 'large';
                        $thumb = $imagen['sizes'][ $size ];
                        $width = $imagen['sizes'][ $size . '-width' ];
                        $height = $imagen['sizes'][ $size . '-height' ];
                        ?>
                        <img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" />
                        <?php endif; ?>
                        <figcaption>
                            <h2><?php the_sub_field('titulo'); ?></h2>
                            <ul class="actions">
                                <li>
                                    <?php $link_boton = get_sub_field( 'link_boton' ); ?>
                                    <?php if ( $link_boton ) : ?>
                                    <a href="<?php echo esc_url( $link_boton['url'] ); ?>" class="button more-info" target="<?php echo esc_attr( $link_boton['target'] ); ?>">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg>
                                        <span><?php echo esc_html( $link_boton['title'] ); ?></span>
                                    </a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <?php endwhile; ?>
		        <?php endif; ?>
                <?php if ( have_rows( 'box_2' ) ) : ?>
                <?php while ( have_rows( 'box_2' ) ) : the_row(); ?>
                <li class="icon col-grid col-md-4">
                    <figure>
                    <?php
                    $imagen = get_sub_field('imagen_2');
                    if( $imagen ):
                        // Image variables.
                        $url = $imagen['url'];
                        $title = $imagen['title'];
                        $alt = $imagen['alt'];
                        // Thumbnail size attributes.
                        $size = 'large';
                        $thumb = $imagen['sizes'][ $size ];
                        $width = $imagen['sizes'][ $size . '-width' ];
                        $height = $imagen['sizes'][ $size . '-height' ];
                        ?>
                        <img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" />
                        <?php endif; ?>
                        <figcaption>
                            <h2><?php the_sub_field('titulo'); ?></h2>
                            <ul class="actions">
                                <li>
                                    <?php $link_boton = get_sub_field( 'link_boton' ); ?>
                                    <?php if ( $link_boton ) : ?>
                                    <a href="<?php echo esc_url( $link_boton['url'] ); ?>" class="button more-info" target="<?php echo esc_attr( $link_boton['target'] ); ?>">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg>
                                        <span><?php echo esc_html( $link_boton['title'] ); ?></span>
                                    </a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <?php endwhile; ?>
		        <?php endif; ?>
                <?php if ( have_rows( 'box_3' ) ) : ?>
                <?php while ( have_rows( 'box_3' ) ) : the_row(); ?>
                <li class="icon col-grid col-md-4">
                    <figure>
                    <?php
                    $imagen = get_sub_field('imagen_3');
                    if( $imagen ):
                        // Image variables.
                        $url = $imagen['url'];
                        $title = $imagen['title'];
                        $alt = $imagen['alt'];
                        // Thumbnail size attributes.
                        $size = 'large';
                        $thumb = $imagen['sizes'][ $size ];
                        $width = $imagen['sizes'][ $size . '-width' ];
                        $height = $imagen['sizes'][ $size . '-height' ];
                        ?>
                        <img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" />
                        <?php endif; ?>
                        <figcaption>
                            <h2><?php the_sub_field('titulo'); ?></h2>
                            <ul class="actions">
                                <li>
                                    <?php $link_boton = get_sub_field( 'link_boton' ); ?>
                                    <?php if ( $link_boton ) : ?>
                                    <a href="<?php echo esc_url( $link_boton['url'] ); ?>" class="button more-info" target="<?php echo esc_attr( $link_boton['target'] ); ?>">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg>
                                        <span><?php echo esc_html( $link_boton['title'] ); ?></span>
                                    </a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <?php endwhile; ?>
		        <?php endif; ?>
            <?php endwhile; ?>
            <?php endif; ?>
            </ul>
            
        </section>
        
        
        <section class="container">
            <div class="copy">
                <div class="grid">
                    <div class="col-12">
                        <h1><?php the_field('titulo'); ?></h1>

                        <?php the_content() ?>
                        
                    </div>
                </div>
            </div>
        </section>
        
        
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

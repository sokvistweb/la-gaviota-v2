<?php /* Template Name: Page Prices */ get_header(); ?>

    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,6)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="container container-padding" id="post-<?php the_ID(); ?>">
            <div class="copy copy-page copy-prices">

				<div class="cd-pricing-container cd-has-margins">
                    <div class="cd-pricing-switcher">
                        <p class="fieldset">
                            <input type="radio" name="duration-1" value="monthly" id="monthly-1" checked>
                            <label for="monthly-1">Camping</label>
                            <input type="radio" name="duration-1" value="yearly" id="yearly-1">
                            <label for="yearly-1">Bungalow/Mobilhome</label>
                            <span class="cd-switch"></span>
                        </p>
                    </div> <!-- .cd-pricing-switcher -->

                    <?php the_content(); ?>
                    
                </div> <!-- .cd-pricing-container -->

            </div>
        </section>
		<?php endwhile; ?>
		<?php else: ?>
        <section class="container container-padding" id="post-<?php the_ID(); ?>">
            <div class="copy copy-page entry-content">

                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                
            </div>
        </section>
		<?php endif; ?>
            
        <?php get_template_part( 'templates/content', 'booking' ); ?>
        
        
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

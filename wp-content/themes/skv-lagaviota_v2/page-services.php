<?php /* Template Name: Page Services */ get_header(); ?>

    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,6)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        
        <section class="container cards">
            <div class="grid">
                
                <?php if (have_posts()) : ?>
                <?php query_posts(array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => $post->ID, 'orderby' => 'menu_order', 'order' => 'ASC' )); ?>
                <?php while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <?php if ( get_field( 'slider_imagenes' ) ): // if checkbox for slider is checked ?>
                    <ul id="cards-slider" class="cards_slider">
                        <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <li><?php the_post_thumbnail('medium', array('class' => 'flex-img')); ?></li>
                        <?php endif; ?>
                        <!-- ACF -->
                        <?php $image = get_field('img_slider_1');
                        if( !empty($image) ): ?>
                        <li><img class="flex-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_2');
                        if( !empty($image) ): ?>
                        <li><img class="flex-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_3');
                        if( !empty($image) ): ?>
                        <li><img class="flex-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('img_slider_4');
                        if( !empty($image) ): ?>
                        <li><img class="flex-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <!-- /ACF -->
                    </ul>
                    <?php else: // field_name returned false ?>
                    <!-- post thumbnail -->
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium', array('class' => 'flex-img')); // Declare pixel size you need inside the array ?>
                    <?php endif; ?>
                    <!-- /post thumbnail -->
                    <?php endif; // end of if field_name logic ?>
                    
                    
                    <div class="card-body">
                        <h2><?php the_title() ?></h2>
                        <?php the_excerpt() ?>
                    </div>
                    
                    <?php if ( get_field( 'boton_mas_info' ) ): ?>
                    <div class="flex-footer">
                        <a href="<?php the_permalink(); ?>" class="button is-blue" title="<?php the_title() ?>">Más info</a>
                    </div>
                    <?php else: // field_name returned false ?>
                    <!-- Nothing to display -->
                    <?php endif; // end of if field_name logic ?>
                    
                </div>
                <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>
                
            </div>
        </section>
		
            
        <?php get_template_part( 'templates/content', 'reviews' ); ?>
        
        
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

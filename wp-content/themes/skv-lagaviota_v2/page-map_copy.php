<?php /* Template Name: Page Map copy */ get_header('map'); ?>

    <main role="main" class="page">
        
        <!--<section class="container heading">
            
        </section>-->
                
        <div class="container-map">
            <div class="media-map">
                <img class="media-bg" src="<?php echo get_template_directory_uri(); ?>/assets/images/planol-lagaviota.jpg" alt="Camping La Gaviota map" width="1600" height="948">
                
                
                <div class="hotspots">
                    <a class="morph-btn hotspot" href="#modal-1" data-toggle="adaptive-modal"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></a>
                    <a class="morph-btn hotspot" href="#modal-2" data-remote="true" data-toggle="adaptive-modal"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></a>
                    <a class="morph-btn hotspot" href="#modal-3" data-toggle="adaptive-modal"></a>
                    
                    <!--<a class="hotspot fade" href="#modal-1" rel="modal:open"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></a>
                    <a class="hotspot fade" href="<?php echo  home_url(); ?>/percelas-alojamientos/mobilhome-llevant/" rel="modal:open"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></a>
                    <a class="hotspot fade" href="#modal-3" rel="modal:open"><svg class="svg-icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs-map.svg#icon-camera"></use></svg></a>-->
                </div>
            </div>
        </div>
            
        <div class="hotspots-content">
            <div class="hotspot-content morph-modal" id="modal-2">
                <div class="content-map modal-content">
                    <figure class="">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/accommodation/mobilhome-llevant-1.jpg"/ alt="Mobilhome LLevant" width="1400" height="931">
                        <figcaption>Torquent mi nullam conubia vestibulum adipiscing</figcaption>
                    </figure>
                </div>
                <button class="close-modal">Close</button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="modal-1">
                <div class="content-map modal-content">
                    <?php query_posts('post_type=page&name=mobilhome-llevant'); while (have_posts ()): the_post(); ?>
                    <section class="container single-accommodation" id="post-<?php the_ID(); ?>">
                        <div class="grid">
                            <div class="col-md-7 col-grid">
                                <ul id="accommodation-gallery" class="accom-gallery">
                                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                                    <li data-thumb="<?php the_post_thumbnail_url('thumbnail'); ?>"><?php the_post_thumbnail('large'); ?></li>
                                    <?php endif; ?>
                                    <!-- ACF -->
                                    <?php $image = get_field('imagen_slider_1');
                                    if( !empty($image) ): ?>
                                    <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                                    <?php endif; ?>
                                    <?php $image = get_field('imagen_slider_2');
                                    if( !empty($image) ): ?>
                                    <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                                    <?php endif; ?>
                                    <?php $image = get_field('imagen_slider_3');
                                    if( !empty($image) ): ?>
                                    <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                                    <?php endif; ?>
                                    <?php $image = get_field('imagen_slider_4');
                                    if( !empty($image) ): ?>
                                    <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                                    <?php endif; ?>
                                    <?php $image = get_field('imagen_slider_5');
                                    if( !empty($image) ): ?>
                                    <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                                    <?php endif; ?>
                                    <?php $image = get_field('imagen_slider_6');
                                    if( !empty($image) ): ?>
                                    <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="col-md-5 col-grid">
                                <div class="content-accom entry-content">

                                    <?php the_content(); ?>

                                    <?php if( get_field('link_inventario') || get_field('texto_inventario') ): ?>
                                    <p><a href="<?php the_field('link_inventario'); ?>" class="household" target="_blank"><span><?php the_field('texto_inventario'); ?></span><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-alt"></use></svg></a></p>
                                    <?php endif; ?>

                                    <?php if( get_field('link_reservar') || get_field('texto_reservar') ): ?>
                                    <a href="<?php the_field('link_reservar'); ?>" class="button btn-icon" target="_blank"><?php the_field('texto_reservar'); ?><svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                                    <?php endif; ?>

                                    <?php if( get_field('dudas') ): ?>
                                    <p><?php the_field('dudas'); ?></p>
                                    <?php endif; ?>

                                    <div class="share-icons">
                                        <p>Comparte</p>
                                        <ul class="social-media share">
                                            <li>
                                                <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg>
                                                    <span class="label">Twitter</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="facebook" target="_blank">
                                                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg>
                                                    <span class="label">Facebook</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="Comparte en Instagram" class="instagram" target="_blank">
                                                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg>
                                                    <span class="label">Instagram</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                                    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                                    <span class="label">WhatsApp</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div><!-- /.share-icons -->
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php endwhile; ?>
                </div>
                <button class="close-modal">Close</button><span class="morph-background"></span>
            </div>
            <div class="hotspot-content morph-modal" id="modal-3">
                <div class="content-map modal-content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/accommodation/mobilhome-llevant-1.jpg"/ alt="Mobilhome LLevant" width="1400" height="931">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/accommodation/mobilhome-llevant-1.jpg"/ alt="Mobilhome LLevant" width="1400" height="931">
                        <figcaption>Nostra litora dui parturient enim curae</figcaption>
                    </figure>
                </div>
                <button class="close-modal">Close</button><span class="morph-background"></span>
            </div>
        </div>
            
    </main>


<?php get_footer('map'); ?>

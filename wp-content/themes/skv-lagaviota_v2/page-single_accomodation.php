<?php /* Template Name: Page Single Accomodation */ get_header(); ?>

    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1><?php the_title(); ?></h1>
        </section>
        
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="container single-accommodation" id="post-<?php the_ID(); ?>">
            <div class="grid">
                <div class="col-md-7 col-grid">
                    <ul id="accommodation-gallery" class="accom-gallery">
                        <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <li data-thumb="<?php the_post_thumbnail_url('thumbnail'); ?>"><?php the_post_thumbnail('large'); ?></li>
                        <?php endif; ?>
                        <!-- ACF -->
                        <?php $image = get_field('imagen_slider_1');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('imagen_slider_2');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('imagen_slider_3');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('imagen_slider_4');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('imagen_slider_5');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                        <?php $image = get_field('imagen_slider_6');
                        if( !empty($image) ): ?>
                        <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-5 col-grid">
                    <div class="content-accom entry-content">
                        
                        <?php the_content(); ?>
                        
                        <?php if( get_field('link_inventario') || get_field('texto_inventario') ): ?>
                        <p><a href="<?php the_field('link_inventario'); ?>" class="household" target="_blank"><span><?php the_field('texto_inventario'); ?></span><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-alt"></use></svg></a></p>
                        <?php endif; ?>
                        
                        <?php if( get_field('link_reservar') || get_field('texto_reservar') ): ?>
                        <a href="<?php the_field('link_reservar'); ?>" class="button btn-icon" target="_blank"><?php the_field('texto_reservar'); ?><svg class="icon-open"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-top-right"></use></svg></a>
                        <?php endif; ?>

                        <?php if( get_field('dudas') ): ?>
                        <p><?php the_field('dudas'); ?></p>
                        <?php endif; ?>

                        <div class="share-icons">
                            <p>Comparte</p>
                            <ul class="social-media share">
                                <li>
                                    <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg>
                                        <span class="label">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="facebook" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg>
                                        <span class="label">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Comparte en Instagram" class="instagram" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg>
                                        <span class="label">Instagram</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                        <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                        <span class="label">WhatsApp</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.share-icons -->
                    </div>
                </div>
            </div>
        </section>
		<?php endwhile; ?>
		<?php else: ?>
        <section class="container container-padding" id="post-<?php the_ID(); ?>">
            <div class="copy entry-content">
                
                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                
            </div>
        </section>
		<?php endif; ?>
        
        
        <!-- Related: show template depending on the page we are -->
        <?php if( is_page('mobilhome-llevant') ) { ?>
            <?php get_template_part( 'templates/content', 'related_llevant' ); ?>
        <?php } ?>
        <?php if( is_page('mobilhome-xaloc') ) { ?>
            <?php get_template_part( 'templates/content', 'related_xaloc' ); ?>
        <?php } ?>
        <?php if( is_page('mobilhome-tamariu') ) { ?>
            <?php get_template_part( 'templates/content', 'related_tamariu' ); ?>
        <?php } ?>
        <?php if( is_page('bungalow-morea') ) { ?>
            <?php get_template_part( 'templates/content', 'related_morea' ); ?>
        <?php } ?>
        <?php if( is_page('parcelas') ) { ?>
            <?php get_template_part( 'templates/content', 'related_parcelas' ); ?>
        <?php } ?>
        <!-- / End Related products -->
            
            
        <?php get_template_part( 'templates/content', 'reviews' ); ?>
            
            
        <?php get_sidebar(); ?>
        
        
    </main>


<?php get_footer(); ?>

    <!-- Cookies popup -->
    <div class="eupopup-container eupopup-container-bottomleft"> 
        <div class="eupopup-markup">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <div class="eupopup-head">Este sitio web usa cookies</div> 
            <div class="eupopup-body">Si continúa navegando, consideramos que acepta su uso.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Aceptar</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Más info</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <div class="eupopup-head">Aquesta web fa servir cookies</div> 
            <div class="eupopup-body">Si continua navegant, considerem que accepta el seu ús. </div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Acceptar</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Més info</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <div class="eupopup-head">This website is using cookies</div> 
            <div class="eupopup-body">By using this website, you agree to this.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='de'): ?>
            <div class="eupopup-head">This website is using cookies</div> 
            <div class="eupopup-body">By using this website, you agree to this.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='fr'): ?>
            <div class="eupopup-head">This website is using cookies</div> 
            <div class="eupopup-body">By using this website, you agree to this.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='nl'): ?>
            <div class="eupopup-head">This website is using cookies</div> 
            <div class="eupopup-body">By using this website, you agree to this.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
            </div> 
            <?php endif; ?>
            <?php } ?>

            <div class="clearfix"></div> 
            <a href="#" class="eupopup-closebutton">
                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
            </a> 
        </div> 
    </div><!-- /Cookies popup -->


    <!--<div class="pcr-test">
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='es'): ?>
        <p>Test PCR para nuestros clientes</p>
        <a href="<?php echo home_url(); ?>/protocolos-covid19" class="plus-info" title="Ver mas informacion">+ info</a>
        <a href="#" class="close-btn">
            <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
        </a>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <p>Test PCR per als nostres clients</p>
        <a href="<?php echo home_url(); ?>/protocolos-covid19" class="plus-info" title="Ver mas informacion">+ info</a>
        <a href="#" class="close-btn">
            <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
        </a>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        <p>PCR tests for our customers</p>
        <a href="<?php echo home_url(); ?>/protocolos-covid19" class="plus-info" title="Ver mas informacion">+ info</a>
        <a href="#" class="close-btn">
            <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
        </a>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='de'): ?>
        <p>PCR-Test für unsere Kunden</p>
        <a href="<?php echo home_url(); ?>/protocolos-covid19" class="plus-info" title="Ver mas informacion">+ info</a>
        <a href="#" class="close-btn">
            <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
        </a>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='fr'): ?>
        <p>Test PCR pour nos clients</p>
        <a href="<?php echo home_url(); ?>/protocolos-covid19" class="plus-info" title="Ver mas informacion">+ info</a>
        <a href="#" class="close-btn">
            <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
        </a>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='nl'): ?>
        <p>PCR test voor onze klanten</p>
        <a href="<?php echo home_url(); ?>/protocolos-covid19" class="plus-info" title="Ver mas informacion">+ info</a>
        <a href="#" class="close-btn">
            <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
        </a>
        <?php endif; ?>
        <?php } ?>
    </div>-->


    <footer>
        <div class="container footer">
            <div class="grid align-end">
                <div class="col-4">
                    <address>
                        Carretera de la Platja, s/n<br>
                        17470 Sant Pere Pescador,<br>
                        Girona
                    </address>
                    <a href="tel:0034972520569">+34 972 520 569</a>
                    <ul class="social-media">
                        <li><a href="https://www.instagram.com/campinglagaviota/" title="Camping La Gaviota en Instagram" target="_blank"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg><span>Instagram</span></a></li>
                        <li><a href="https://www.facebook.com/gaviotaCAMPING" title="Camping La Gaviota en Facebook" target="_blank"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg><span>Facebook</span></a></li>
                        <!--<li><a href="https://twitter.com/gaviotacamping" title="Camping La Gaviota en Twitter" target="_blank"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg><span>Pinterest</span></a></li>-->
                        <li><a href="https://www.youtube.com/channel/UCe-DYxwoXxqTo5YGf1ISXUQ" title="Camping La Gaviota en YouTube" target="_blank"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-social-youtube"></use></svg><span>Pinterest</span></a></li>
                    </ul>
                </div>
                <div class="col-4">
                    <?php footer_nav(); ?>
                </div>
                <div class="col-4">
                    <?php legal_nav(); ?>
                </div>
            </div> <!-- /.grid -->
        </div><!-- /.container -->
        
        <div class="container sub-footer">
            <div class="grid"> 
                <div class="col-12">
                    <img class="multi-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logos.jpg" alt="Logos" width="837" height="100">
                    <ul>
                        <li>RTC KG-000039</li>
                        <li><a href="https://www.lagaviota.com" title="Copy Right">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></a></li>
                        <li>Web by <a href="http://www.sokvist.com/" title="Sokvist, Web &amp; Marketing">Sokvist</a></li>
                    </ul>
                </div>
            </div> <!-- /.grid -->
        </div><!-- /.container -->
        
        <svg class="wave-f1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 1000"><path class="p2" d="M1928 133.9C1232.3 465.9 602.6-353.5-4.5 343.3l-1.7 661.1H1928V133.9z"/><path class="p1" d="M1928 119.8C1276.1 410.8 631.8-420-3.5 330.5v12.8c607-696.9 1235.9 122.5 1931.5-209.5v-14z"/></svg>
        
        <svg class="wave-f2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 400"><path d="M1929 193.6c-642.1-41-1290.8-351.4-1933-65.1v277h1933V193.6z"/></svg>
        
        <a href="#top" id="go-top"><span>Back to top</span></a>
        
    </footer>


    <?php get_template_part( 'templates/content', 'booking' ); ?>
    
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>


    <?php wp_footer(); ?>


    <!-- Widget Master ASP booking motor -->
    <script>
    window.onload = function masterbookings_widget_init() {
        var widget1 = new MasterWidget("widgetBookingContainer", {
            lang: "<?php if(function_exists('qtranxf_getLanguage')) { ?><?php if (qtranxf_getLanguage()=='es'): ?>es<?php endif; ?><?php if (qtranxf_getLanguage()=='ca'): ?>ca<?php endif; ?><?php if (qtranxf_getLanguage()=='en'): ?>en<?php endif; ?><?php if (qtranxf_getLanguage()=='de'): ?>de<?php endif; ?><?php if (qtranxf_getLanguage()=='fr'): ?>fr<?php endif; ?><?php if (qtranxf_getLanguage()=='nl'): ?>nl<?php endif; ?><?php } ?>",
            dropdown: true,
            idProperty: 346,
            url: 'https://booking.lagaviota.com?ra=google'
        });
    }
    </script>


	</body>
</html>

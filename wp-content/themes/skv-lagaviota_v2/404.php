<?php get_header(); ?>



	<main role="main">
        
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,6)); ?>">
            <div class="overlay"></div>
        </section>
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>

		<!-- section -->
		<section class="container heading">

			<!-- article -->
			<article id="post-404">



				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>

				<h2>

					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>

				</h2>



			</article>
			<!-- /article -->


		</section>
		<!-- /section -->

	</main>



<?php get_sidebar(); ?>



<?php get_footer(); ?>


<?php include("header.php"); ?>



    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1>Página genérica</h1>
        </section>
        
        
        <section class="container container-padding">
            <div class="copy entry-content">
            
                <h2>Tempus egestas (h2)</h2>
                
                <h3>Morbi tincidunt ornare (h3)</h3>
            
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In fermentum posuere urna nec tincidunt praesent semper feugiat. Augue lacus viverra vitae congue eu consequat ac felis donec. Vitae justo eget magna fermentum iaculis. Ultrices neque ornare aenean euismod elementum nisi quis eleifend quam. Quis vel eros donec ac. Adipiscing at in tellus integer feugiat scelerisque varius. Donec ultrices tincidunt arcu non. Tempor orci eu lobortis elementum nibh tellus molestie nunc. Morbi tincidunt ornare massa eget egestas. Massa sed elementum tempus egestas sed sed risus pretium quam.</p>
                
                <div class="grid">
                    <div class="col-6">
                        <ul>
                            <li>consectetur</li>
                            <li>adipiscing</li>
                            <li>elit</li>
                            <li>eiusmod</li>
                            <li>tempor</li>
                            <li>incididunt</li>
                            <li>malesuada</li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ol>
                            <li>labore</li>
                            <li>dolore</li>
                            <li>magna</li>
                            <li>aliqua</li>
                            <li>Mauris</li>
                            <li>vitae</li>
                            <li>ultricies</li>
                        </ol>
                    </div>
                </div>

                <p>Morbi tincidunt augue interdum velit euismod in pellentesque massa. Tempus egestas sed sed risus pretium. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. A diam sollicitudin tempor id eu nisl. Massa sapien faucibus et molestie ac feugiat. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Etiam dignissim diam quis enim lobortis. Eu nisl nunc mi ipsum faucibus. Dignissim enim sit amet venenatis urna cursus eget nunc scelerisque. Ac odio tempor orci dapibus ultrices in iaculis nunc sed. Massa id neque aliquam vestibulum morbi blandit. Purus in massa tempor nec feugiat nisl pretium fusce. Quam viverra orci sagittis eu volutpat. Id ornare arcu odio ut. Et leo duis ut diam quam nulla. Eu facilisis sed odio morbi quis commodo. Massa id neque aliquam vestibulum morbi blandit cursus. Malesuada proin libero nunc consequat. Quisque non tellus orci ac auctor augue mauris augue neque.</p>

                <p>Massa tempor nec feugiat nisl pretium fusce. Eget nunc lobortis mattis aliquam faucibus. Id nibh tortor id aliquet lectus proin nibh nisl. Faucibus et molestie ac feugiat sed lectus vestibulum. Cursus sit amet dictum sit amet justo. Vulputate odio ut enim blandit. Dolor sed viverra ipsum nunc aliquet bibendum. Purus semper eget duis at tellus at urna condimentum mattis. Scelerisque fermentum dui faucibus in ornare. Urna neque viverra justo nec ultrices dui sapien eget mi. Lectus arcu bibendum at varius. Non blandit massa enim nec. Placerat in egestas erat imperdiet sed euismod nisi. At quis risus sed vulputate odio ut.</p>
            
            </div>
        </section>
        
        
        <?php include("content-reviews.php"); ?>
        
        
        <section class="container pre-footer">
            <div class="grid">
                <div class="col-md-6 col-bleed">
                    <h3>Suscríbete</h3>
                    <div class="mailchimp-wrapper">
                        <p>Infórmate de nuestras ofertas y novedades</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                            <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                    </div>
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                                    </div>
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response"></div>
                                        <div class="response" id="mce-success-response"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                                    <div class="p-accept">
                                    <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="https://www.lagaviota.com/politica-de-privacidad">política de privacidad</a></span>
                                    </div>
                                    <div class="clear">
                                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Ends MailChimp Signup Form -->
                    </div>
                </div>
                <div class="col-md-6 col-bleed">
                    <h3>Cómo llegar</h3>
                    <a href="https://g.page/gaviotaCAMPING?share" class="map" title="Ver el mapa de Google" target="_blank"><span>Ver en Google Maps</span></a>
                    <!--<img class="multi-logo" src="assets/images/google-map-la-gaviota.jpg" alt="Logos" width="1140" height="390">-->
                </div>
            </div>
        </section>
        
        
    </main>
    
    
<?php include("footer.php"); ?>

<?php include("header.php"); ?>



    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1>Servicios</h1>
        </section>
        
        
        <section class="container cards">
            <div class="grid">
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <img class="flex-img" src="assets/images/gastronomia.jpg">
                    <div class="card-body">
                        <h2>Gastronomia</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et sagittis dui. Maecenas rhoncus justo aliquam urna finibus tincidunt et ut mauris.</p>
                    </div>
                    <div class="flex-footer">
                        <a href="#0" class="button is-blue">Más info</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <img class="flex-img" src="assets/images/sanitarios.jpg">
                    <div class="card-body">
                        <h2>Sanitarios</h2>
                        <p>Nunc dignissim in ante sed dapibus. Aenean sit amet aliquet dolor, at auctor erat. Aliquam ornare faucibus leo, vel euismod nisi porta non. Donec at arcu eget enim bibendum facilisis. Etiam urna elit, blandit ut libero placerat, iaculis aliquam erat.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <ul id="cards-slider">
                        <li>
                            <img class="flex-img" src="assets/images/piscina.jpg">
                        </li>
                        <li>
                            <img class="flex-img" src="assets/images/piscina-2.jpg">
                        </li>
                    </ul>
                    <div class="card-body">
                        <h2>Piscina</h2>
                        <p>Aenean sit amet aliquet dolor, at auctor erat. Aliquam ornare faucibus leo, vel euismod nisi porta non. Donec at arcu eget enim bibendum facilisis.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <img class="flex-img" src="assets/images/entretenimientos.jpg">
                    <div class="card-body">
                        <h2>Entretenimientos</h2>
                        <p>Aliquam ornare faucibus leo, vel euismod nisi porta non. Donec at arcu eget enim bibendum facilisis.</p>
                    </div>
                    <div class="flex-footer">
                        <span>Desde 30 €</span>
                        <a href="#0" class="button is-blue">Más info</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-grid">
                    <img class="flex-img" src="assets/images/tienda.jpg">
                    <div class="card-body">
                        <h2>Tienda</h2>
                        <p>Aliquam ornare faucibus leo, vel euismod nisi porta non. Donec at arcu eget enim bibendum facilisis.</p>
                    </div>
                </div>
            </div>
        </section>
        
        
        <section class="container">
            <div class="reviews">
                <div class="grid">
                    <div class="col-12">
                        <ul id="reviews-slider">
                            <li>
                                <blockquote>Camping tranquilo y acogedor, de los pocos que quedan por esta zona sin masificar. A pie de playa.
                                    <span>Ramon Bonvehi</span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>Buenas parcelas con mucha sombra, servicios muy limpios. El próximo verano seguro que vuelvo. 👍👍
                                    <span>Juan José Alvaro</span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>Tranquilo, con todos los servicios, a 100 metros de una playa preciosa con vistas de postal, un consejo, visitar la playa por la tarde noche con luna llena naciente y tomar algo en el chiringuito CHÉ o en la misma playa sobre la arena.... Vistas de película.
                                    <span>Javier Jiménez</span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>Me encanta es pequeño pero muy bien las instalaciones.
                                    <span>Marisa Piazza</span>
                                </blockquote>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        
        
        <section class="container pre-footer">
            <div class="grid">
                <div class="col-md-6 col-bleed">
                    <h3>Suscríbete</h3>
                    <div class="mailchimp-wrapper">
                        <p>Infórmate de nuestras ofertas y novedades</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                            <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                    </div>
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                                    </div>
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response"></div>
                                        <div class="response" id="mce-success-response"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                                    <div class="p-accept">
                                    <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="https://www.lagaviota.com/politica-de-privacidad">política de privacidad</a></span>
                                    </div>
                                    <div class="clear">
                                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Ends MailChimp Signup Form -->
                    </div>
                </div>
                <div class="col-md-6 col-bleed">
                    <h3>Cómo llegar</h3>
                    <a href="https://g.page/gaviotaCAMPING?share" class="map" title="Ver el mapa de Google" target="_blank"><span>Ver en Google Maps</span></a>
                    <!--<img class="multi-logo" src="assets/images/google-map-la-gaviota.jpg" alt="Logos" width="1140" height="390">-->
                </div>
            </div>
        </section>
        
        
    </main>
    
    
<?php include("footer.php"); ?>

/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

jQuery(document).ready(function($) {
	
	
    /**
    Header scrolling
    **/
    $(window).scroll(function(){
        $('header').toggleClass('scrolled', $(this).scrollTop() > 100);
        $('main').toggleClass('scrolled', $(this).scrollTop() > 200);
        $('.container-widget-booking').toggleClass('scrolled', $(this).scrollTop() > 50);
    });
    
    
    
    /* Booking button */
    //// Back To Top
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 70,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_color = 500,
		//grab the "back to top" link
		booking_btn = $('.booking-popup');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? booking_btn.addClass('shows-up') : booking_btn.removeClass('shows-up color-change');
		if( $(this).scrollTop() > offset_color ) { 
			booking_btn.addClass('color-change');
		}
	});
    
    
    
    /*! Mobile main nav
    *   http://codyhouse.co/gem/stretchy-navigation/ */
    if( $('.stretchy-nav').length > 0 ) {
		var stretchyNavs = $('.stretchy-nav');
		
		stretchyNavs.each(function(){
			var stretchyNav = $(this),
				stretchyNavTrigger = stretchyNav.find('.nav-trigger');
			
			stretchyNavTrigger.on('click', function(event){
				event.preventDefault();
				stretchyNav.toggleClass('nav-is-visible');
			});
		});

		$(document).on('click', function(event){
			( !$(event.target).is('.nav-trigger') && !$(event.target).is('.nav-trigger span') ) && stretchyNavs.removeClass('nav-is-visible');
		});
	}
    
    
    
    /*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.9.2
    */
    $('.fade').click(function(event) {
        $(this).modal({
            fadeDuration: 50
        });
        $('.booking-popup').addClass('visible');
        return false;
    });
    
    
    
    
    /**
    To top button
    **/
    $('a[href="#top"]').click(function(){
        $('html, body').animate({scrollTop: 0}, 'slow');
        return false;
    });


    
        
    /**
    Toggle menu
    **/
    $('.toggle-menu').click(function(){
        $('.menu-responsive').slideToggle();
        return false;
    });
    
    
    
    /*! lightslider - v1.1.6 - 2016-10-25
    * https://github.com/sachinchoolur/lightslider
    * Copyright (c) 2016 Sachin N; Licensed MIT */
    $('#reviews-slider').lightSlider({
        adaptiveHeight: true,
        item: 2,
        slideMargin: 0,
        speed: 700,
        loop: true,
        controls: true,
        responsive : [
            {
                breakpoint: 1025,
                settings: {
                    item: 1,
                    slideMove: 1,
                }
            }
        ]
    });
    
    $('#cards-slider').lightSlider({
        item: 1,
        slideMargin: 0,
        speed: 700,
        loop: true,
        controls: true,
        pager: false
    });
    
    $('#accommodation-gallery').lightSlider({
        gallery: true,
        item: 1,
        thumbItem: 5,
        slideMargin: 0,
        galleryMargin: 10,
        thumbMargin: 10,
        speed: 700,
        loop: true
    });
    
    
    
    
    /**
    Pricing table
    **/
    //hide the subtle gradient layer (.cd-pricing-list > li::after) when pricing table has been scrolled to the end (mobile version only)
	checkScrolling($('.cd-pricing-body'));
	$(window).on('resize', function(){
		window.requestAnimationFrame(function(){checkScrolling($('.cd-pricing-body'))});
	});
    
	$('.cd-pricing-body').on('scroll', function(){
		var selected = $(this);
		window.requestAnimationFrame(function(){checkScrolling(selected)});
	});

	function checkScrolling(tables){
		tables.each(function(){
			var table= $(this),
				totalTableWidth = parseInt(table.children('.cd-pricing-features').width()),
		 		tableViewport = parseInt(table.width());
			if( table.scrollLeft() >= totalTableWidth - tableViewport -1 ) {
				table.parent('li').addClass('is-ended');
			} else {
				table.parent('li').removeClass('is-ended');
			}
		});
	}

	//switch from monthly to annual pricing tables
	bouncy_filter($('.cd-pricing-container'));

	function bouncy_filter(container) {
		container.each(function(){
			var pricing_table = $(this);
			var filter_list_container = pricing_table.children('.cd-pricing-switcher'),
				filter_radios = filter_list_container.find('input[type="radio"]'),
				pricing_table_wrapper = pricing_table.find('.cd-pricing-wrapper');

			//store pricing table items
			var table_elements = {};
			filter_radios.each(function(){
				var filter_type = $(this).val();
				table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="'+filter_type+'"]');
			});

			//detect input change event
			filter_radios.on('change', function(event){
				event.preventDefault();
				//detect which radio input item was checked
				var selected_filter = $(event.target).val();

				//give higher z-index to the pricing table items selected by the radio input
				show_selected_items(table_elements[selected_filter]);

				//rotate each cd-pricing-wrapper 
				//at the end of the animation hide the not-selected pricing tables and rotate back the .cd-pricing-wrapper
				
				if( !Modernizr.cssanimations ) {
					hide_not_selected_items(table_elements, selected_filter);
					pricing_table_wrapper.removeClass('is-switched');
				} else {
					pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {		
						hide_not_selected_items(table_elements, selected_filter);
						pricing_table_wrapper.removeClass('is-switched');
						//change rotation direction if .cd-pricing-list has the .cd-bounce-invert class
						if(pricing_table.find('.cd-pricing-list').hasClass('cd-bounce-invert')) pricing_table_wrapper.toggleClass('reverse-animation');
					});
				}
			});
		});
	}
    
	function show_selected_items(selected_elements) {
		selected_elements.addClass('is-selected');
	}

	function hide_not_selected_items(table_containers, filter) {
		$.each(table_containers, function(key, value){
	  		if ( key != filter ) {	
				$(this).removeClass('is-visible is-selected').addClass('is-hidden');

			} else {
				$(this).addClass('is-visible').removeClass('is-hidden is-selected');
			}
		});
	}
    
    
    
    
});



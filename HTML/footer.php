    <footer>
        <div class="container footer">
            <div class="grid align-end">
                <div class="col-4">
                    <address>
                        Carretera de la Platja, s/n<br>
                        17470 Sant Pere Pescador,<br>
                        Girona
                    </address>
                    <a href="tel:0034972520569">+34 972 520 569</a>
                    <ul class="social-media">
                        <li><a href="https://www.instagram.com/bellalolabikinis/"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg><span>Instagram</span></a></li>
                        <li><a href="https://www.facebook.com/BellaLolaBikinis"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg><span>Facebook</span></a></li>
                        <li><a href="https://www.pinterest.es/bella_lola/"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg><span>Pinterest</span></a></li>
                        <li><a href="https://www.pinterest.es/bella_lola/"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-youtube"></use></svg><span>Pinterest</span></a></li>
                    </ul>
                </div>
                <div class="col-4">
                    <ul>
                        <li><a href="page.php">Parcelas & Alojamientos</a></li>
                        <li><a href="page.php">Servicios</a></li>
                        <li><a href="page.php">Ofertas</a></li>
                        <li><a href="page.php">Recepción</a></li>
                        <li><a href="page.php">Inspírate</a></li>
                    </ul>
                </div>
                <div class="col-4">
                    <ul>
                        <li><a href="page.php" title="Aviso Legal">Aviso Legal</a></li>
                        <li><a href="page.php" title="Política de cookie">Política de cookies</a></li>
                        <li><a href="page.php" title="Política de privacidad">Política de privacidad</a></li>
                    </ul>
                </div>
            </div> <!-- /.grid -->
        </div><!-- /.container -->
        
        <div class="container sub-footer">
            <div class="grid"> 
                <div class="col-12">
                    <img class="multi-logo" src="assets/images/logos.jpg" alt="Logos" width="732" height="80">
                    <ul>
                        <li>RTC KG-000039</li>
                        <li><a href="https://www.lagaviota.com" title="Copy Right">© 2020 Camping La Gaviota</a></li>
                        <li><a href="https://www.lagaviota.com/wp-admin/" title="Web Admin">Admin</a></li>
                        <li>Web by <a href="http://www.sokvist.com/" title="Sokvist, Web &amp; Marketing">Sokvist</a></li>
                    </ul>
                </div>
            </div> <!-- /.grid -->
        </div><!-- /.container -->
        
        <svg class="wave-f1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 1000"><path class="p2" d="M1928 133.9C1232.3 465.9 602.6-353.5-4.5 343.3l-1.7 661.1H1928V133.9z"/><path class="p1" d="M1928 119.8C1276.1 410.8 631.8-420-3.5 330.5v12.8c607-696.9 1235.9 122.5 1931.5-209.5v-14z"/></svg>
        
        <svg class="wave-f2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 400"><path d="M1929 193.6c-642.1-41-1290.8-351.4-1933-65.1v277h1933V193.6z"/></svg>
        
        <a href="#top" id="go-top"><span>Back to top</span></a>
        
    </footer>
    
    
    <?php include("booking-engine.php"); ?>
    
    
    
    <script type="text/javascript" src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="assets/js/min/main.min.js"></script>

</body>
</html>

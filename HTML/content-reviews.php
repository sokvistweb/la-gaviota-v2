<section class="container">
    <div class="reviews">
        <div class="grid">
            <div class="col-12">
                <ul id="reviews-slider">
                    <li>
                        <blockquote>Camping tranquilo y acogedor, de los pocos que quedan por esta zona sin masificar. A pie de playa.
                            <span>Ramon Bonvehi</span>
                        </blockquote>
                    </li>
                    <li>
                        <blockquote>Buenas parcelas con mucha sombra, servicios muy limpios. El próximo verano seguro que vuelvo. 👍👍
                            <span>Juan José Alvaro</span>
                        </blockquote>
                    </li>
                    <li>
                        <blockquote>Tranquilo, con todos los servicios, a 100 metros de una playa preciosa con vistas de postal, un consejo, visitar la playa por la tarde noche con luna llena naciente y tomar algo en el chiringuito CHÉ o en la misma playa sobre la arena.... Vistas de película.
                            <span>Javier Jiménez</span>
                        </blockquote>
                    </li>
                    <li>
                        <blockquote>Me encanta es pequeño pero muy bien las instalaciones.
                            <span>Marisa Piazza</span>
                        </blockquote>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
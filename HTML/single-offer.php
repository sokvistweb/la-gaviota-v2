<?php include("header.php"); ?>



    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1>Fin de semana en la playa</h1>
        </section>
        
        
        <section class="container offer-cards single-offer">
            <div class="grid">
                <div class="col-md-9 col-grid">
                    <div class="grid">
                        <div class="col-12 col-grid">
                            <div class="card-full">
                                <img class="flex-img" src="assets/images/piscina-2.jpg">
                                <h2>Oferta de fin de semana</h2>
                                <div class="entry-meta">
                                    <span class="post-date"><time datetime="2020-02-24 15:19">24 de febrero de 2020</time></span>
                                </div>
                                <div class="entry-content offer-content">
                                    <div class="offer-meta">
                                        <div class="offer-date">
                                            <span>De viernes a domingo</span>
                                        </div>
                                        <div class="offer-accom">
                                            <span>Estancia en bungalow para 4 personas</span>
                                        </div>
                                        <div class="offer-price">
                                            <span>Desde 140€</span>
                                        </div>
                                        <div class="offer-extra">
                                            <span>Late checkout (para que podáis disfrutar del domingo con tranquilidad)</span>
                                        </div>
                                    </div>
                                    <p>Restricciones: Oferta válida del 1 de abril al 31 de mayo de 2020, excepto Semana Santa y puentes oficiales. Tasa turística no incluida.</p>
                                    
                                    <h3>Rellena el formulario para hacer tu reserva</h3>
                                    
                                    <div id="contactform" class="contact-form">
                                        <div role="form" class="wpcf7" id="wpcf7-f910-p1279-o1" lang="de-DE" dir="ltr">
                                        <div class="screen-reader-response"></div>
                                        <form action="/fin-de-semana-en-la-playa/#wpcf7-f910-p1279-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                        <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="910">
                                        <input type="hidden" name="_wpcf7_version" value="5.1.6">
                                        <input type="hidden" name="_wpcf7_locale" value="de_DE">
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f910-p1279-o1">
                                        <input type="hidden" name="_wpcf7_container_post" value="1279">
                                        <input type="hidden" name="g-recaptcha-response" value="">
                                        </div>
                                        <p>Nombre y apellido (obligatorio)<br>
                                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </p>
                                        <p>Email (obligatorio)<br>
                                            <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </p>
                                        <p>Fechas (obligatorio) Por favor, indícanos la fecha de entrada y de salida.<br>
                                            <span class="wpcf7-form-control-wrap fechas"><textarea name="fechas" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </p>
                                        <p>Número de huéspedes (obligatorio). Por favor, indica cuántos adultos y cuántos niños os alojaréis.<br>
                                            <span class="wpcf7-form-control-wrap personen"><textarea name="personen" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </p>
                                        <p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit"></p>
                                        <div class="wpcf7-response-output wpcf7-display-none"></div></form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-grid">
                    <div class="grid">
                        <div class="col-12 col-grid">
                            <div class="card-body">
                                <a href="single-offer.php"><img class="flex-img" src="assets/images/piscina.jpg"></a>
                                <h2><a href="single-offer.php">Nunc et sagittis</a></h2>
                                <div class="entry-meta">
                                    <span class="post-date"><time datetime="2020-02-24 15:19">24 de febrero de 2020</time></span>
                                </div>
                                <div class="entry-content clearfix">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et sagittis dui. Maecenas rhoncus justo aliquam urna finibus tincidunt et ut mauris.
                                </div>
                            </div>
                            <div class="flex-footer">
                                <a href="single-offer.php" class="button">Reservar</a>
                            </div>
                        </div>
                        <div class="col-12 col-grid">
                            <div class="card-body">
                                <a href="single-offer.php"><img class="flex-img" src="assets/images/parcelas.jpg"></a>
                                <h2><a href="single-offer.php">Maecenas</a></h2>
                                <div class="entry-meta">
                                    <span class="post-date"><time datetime="2020-02-24 15:19">24 de febrero de 2020</time></span>
                                </div>
                                <div class="entry-content clearfix">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et sagittis dui. Maecenas rhoncus justo aliquam urna finibus tincidunt et ut mauris.
                                </div>
                            </div>
                            <div class="flex-footer">
                                <a href="single-offer.php" class="button">Reservar</a>
                            </div>
                        </div>
                        <div class="col-12 col-grid">
                            <div class="card-body">
                                <a href="single-offer.php"><img class="flex-img" src="assets/images/sanitarios.jpg"></a>
                                <h2><a href="single-offer.php">Donec at arcu</a></h2>
                                <div class="entry-meta">
                                    <span class="post-date"><time datetime="2020-02-24 15:19">24 de febrero de 2020</time></span>
                                </div>
                                <div class="entry-content clearfix">
                                    Aliquam ornare faucibus leo, vel euismod nisi porta non. Donec at arcu eget enim bibendum facilisis.
                                </div>
                            </div>
                            <div class="flex-footer">
                                <a href="single-offer.php" class="button">Reservar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        <section class="container">
            <div class="reviews">
                <div class="grid">
                    <div class="col-12">
                        <ul id="reviews-slider">
                            <li>
                                <blockquote>Camping tranquilo y acogedor, de los pocos que quedan por esta zona sin masificar. A pie de playa.
                                    <span>Ramon Bonvehi</span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>Buenas parcelas con mucha sombra, servicios muy limpios. El próximo verano seguro que vuelvo. 👍👍
                                    <span>Juan José Alvaro</span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>Tranquilo, con todos los servicios, a 100 metros de una playa preciosa con vistas de postal, un consejo, visitar la playa por la tarde noche con luna llena naciente y tomar algo en el chiringuito CHÉ o en la misma playa sobre la arena.... Vistas de película.
                                    <span>Javier Jiménez</span>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>Me encanta es pequeño pero muy bien las instalaciones.
                                    <span>Marisa Piazza</span>
                                </blockquote>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        
        
        <section class="container pre-footer">
            <div class="grid">
                <div class="col-md-6 col-bleed">
                    <h3>Suscríbete</h3>
                    <div class="mailchimp-wrapper">
                        <p>Infórmate de nuestras ofertas y novedades</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                            <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                    </div>
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                                    </div>
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response"></div>
                                        <div class="response" id="mce-success-response"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                                    <div class="p-accept">
                                    <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="https://www.lagaviota.com/politica-de-privacidad">política de privacidad</a></span>
                                    </div>
                                    <div class="clear">
                                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Ends MailChimp Signup Form -->
                    </div>
                </div>
                <div class="col-md-6 col-bleed">
                    <h3>Cómo llegar</h3>
                    <a href="https://g.page/gaviotaCAMPING?share" class="map" title="Ver el mapa de Google" target="_blank"><span>Ver en Google Maps</span></a>
                    <!--<img class="multi-logo" src="assets/images/google-map-la-gaviota.jpg" alt="Logos" width="1140" height="390">-->
                </div>
            </div>
        </section>
        
        
    </main>
    
    
<?php include("footer.php"); ?>

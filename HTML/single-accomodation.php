<?php include("header.php"); ?>



    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1>Mobilhome <span>Xaloc</span></h1>
        </section>
        
        
        <section class="container single-accommodation">
            <div class="grid">
                <div class="col-md-7 col-grid">
                    <ul id="accommodation-gallery" class="accom-gallery">
                        <li data-thumb="assets/images/accommodation/th_mobilhome-llevant-1.jpg"><img width="900" height="599" src="assets/images/accommodation/mobilhome-llevant-1.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                        <li data-thumb="assets/images/accommodation/th_mobilhome-llevant-2.jpg"><img width="900" height="599" src="assets/images/accommodation/mobilhome-llevant-2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                        <li data-thumb="assets/images/accommodation/th_mobilhome-llevant-3.jpg"><img width="900" height="599" src="assets/images/accommodation/mobilhome-llevant-3.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                        <li data-thumb="assets/images/accommodation/th_mobilhome-llevant-4.jpg"><img width="900" height="599" src="assets/images/accommodation/mobilhome-llevant-4.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                        <li data-thumb="assets/images/accommodation/th_mobilhome-llevant-5.jpg"><img width="900" height="599" src="assets/images/accommodation/mobilhome-llevant-5.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                        <li data-thumb="assets/images/accommodation/th_mobilhome-llevant-6.jpg"><img width="900" height="599" src="assets/images/accommodation/mobilhome-llevant-6.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                    </ul>
                </div>
                <div class="col-md-5 col-grid">
                    <div class="content-accom">
                        <h2 class="product_title entry-title">Reserva tu mobilhome</h2>
                        
                        <div class="entry-content">
                            <p>Situado al final del camping, en la parte más tranquila.</p>

                            <p>Equipado para 4 personas.</p>

                            <ul>
                                <li>Agua caliente y electricidad (220w)</li>
                                <li>Salón/cocina a gas&nbsp;con extractor de humos, nevera y microondas</li>
                                <li>Baño completo: lavabo, ducha, WC, secador de pelo</li>
                                <li>Habitación doble con cama&nbsp;de matrimonio</li>
                                <li>Habitación doble con dos camas individuales</li>
                                <li>Sábanas y mantas</li>
                                <li>Terraza amueblada con mesa y 4 sillas</li>
                            </ul>
                            
                            <p>Si quieres que te proporcionemos toallas, el precio es 5€/persona y semana.</p>

                            <p>¿Qué necesitas traer de casa? Comprueba aquí lo que encontrarás en el alojamiento:</p>
                            
                            <p><a href="https://www.lagaviota.com/wp-content/uploads/2016/02/inventario-ES.pdf" target="_blank">INVENTARIO</a></p>
                            
                            <a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a>

                            <p>¿Tienes dudas? ¿Quieres que te asesoremos? escríbenos a <a href="mailto:info@lagaviota.com">info@lagaviota.com</a></p>
                        </div>

                        <div class="share-icons">
                            <p>Comparte</p>
                            <ul class="social-media share">
                                <li>
                                    <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-twitter"></use></svg>
                                        <span class="label">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="facebook" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-facebook"></use></svg>
                                        <span class="label">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Comparte en Instagram" class="instagram" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-instagram"></use></svg>
                                        <span class="label">Instagram</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                        <span class="label">WhatsApp</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.share-icons -->
                    </div>
                </div>
            </div>
        </section>
        
        
        <section class="container more-accom">
            <h2>También te puede interesar</h2>
            <ul class="features grid align-center">
                <li class="icon col-grid col-md-3">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/mobilhome-xaloc.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h3>Mobilhome <span>Xaloc</span></h3>
                            <ul class="actions">
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Más info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
                <li class="icon col-grid col-md-3">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/mobilhome-tamariu.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h3>Mobilhome <span>Tamariu</span></h3>
                            <ul class="actions">
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
                <li class="icon col-grid col-md-3">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/bungalow-morea.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h3>Bungalow <span>Morea</span></h3>
                            <ul class="actions">
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <li class="icon col-grid col-md-3">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/parcelas.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h3><span>Parcelas</span></h3>
                            <ul class="actions">
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
            </ul>
            
        </section>
        
        
        <?php include("content-reviews.php"); ?>
        
        
        <section class="container pre-footer">
            <div class="grid">
                <div class="col-md-6 col-bleed">
                    <h3>Suscríbete</h3>
                    <div class="mailchimp-wrapper">
                        <p>Infórmate de nuestras ofertas y novedades</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                            <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                    </div>
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                                    </div>
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response"></div>
                                        <div class="response" id="mce-success-response"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                                    <div class="p-accept">
                                    <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="https://www.lagaviota.com/politica-de-privacidad">política de privacidad</a></span>
                                    </div>
                                    <div class="clear">
                                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Ends MailChimp Signup Form -->
                    </div>
                </div>
                <div class="col-md-6 col-bleed">
                    <h3>Cómo llegar</h3>
                    <a href="https://g.page/gaviotaCAMPING?share" class="map" title="Ver el mapa de Google" target="_blank"><span>Ver en Google Maps</span></a>
                    <!--<img class="multi-logo" src="assets/images/google-map-la-gaviota.jpg" alt="Logos" width="1140" height="390">-->
                </div>
            </div>
        </section>
        
        
    </main>
    
    
<?php include("footer.php"); ?>

<?php include("header.php"); ?>



    <main role="main" class="page">
       
        <section class="bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
            <div class="overlay"></div>
        </section>
        
        
        <section class="waves">
            <svg class="wave-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-5 428.5h1927.3s0-419.3 1.3-420.2C1272.8 536.1 629.4-441.8-3.4 305.7L-5 428.5z"/></svg>

            <svg class="wave-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 424"><path d="M-2.8 432h1924.2s0-426.2 1.3-427C1222.7 556.4 598-387.1-2.3 302l-.5 130z"/></svg>
            
            <svg class="wave-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 284"><path d="M1925 183.5C1287.3 381.3 637.6-257.3-4 144.2V290h1929V183.5z"/></svg>
        </section>
        
        
        <section class="container heading">
            <h1>Parcelas & Alojamientos</h1>
        </section>
        
        
        <section class="container">
            
            <ul class="features grid align-center">
                <li class="icon col-grid col-md-4">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/mobilhome-llevant.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h2>Mobilhome <span>Llevant</span></h2>
                            <ul class="actions">
                                <li><span class="min-price">Des de 160€</span></li>
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
                <li class="icon col-grid col-md-4">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/mobilhome-xaloc.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h2>Mobilhome <span>Xaloc</span></h2>
                            <ul class="actions">
                                <li><span class="min-price">Des de 160€</span></li>
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Más info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
                <li class="icon col-grid col-md-4">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/mobilhome-tamariu.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h2>Mobilhome <span>Tamariu</span></h2>
                            <ul class="actions">
                                <li><span class="min-price">Des de 160€</span></li>
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
                <li class="icon col-grid col-md-4">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/bungalow-morea.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h2>Bungalow <span>Morea</span></h2>
                            <ul class="actions">
                                <li><span class="min-price">Des de 160€</span></li>
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                <li class="icon col-grid col-md-4">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/parcelas.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h2><span>Parcelas</span></h2>
                            <ul class="actions">
                                <li><span class="min-price">Des de 160€</span></li>
                                <li><a href="single-accomodation.php" class="button more-info" title="Ver información del alojamiento"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-more-info"></use></svg><span>Més info</span></a></li>
                                <li><a href="https://booking.lagaviota.com/search" class="button" title="Reserva tu alojamiento" target="_blank">Reservar</a></li>
                            </ul>
                        </figcaption>			
                    </figure>
                </li>
                <li class="icon col-grid col-md-4 is-map">
                    <figure>
                        <img class="lazy" data-src="" src="assets/images/plano-del-camping.jpg" alt="" width="600" height="400" />
                        <figcaption>
                            <h2><span>Plano</span> del Cámping</h2>
                            <a href="page.php" class="" title="Ver plano del cámping"><span>Més info</span></a>
                        </figcaption>			
                    </figure>
                </li>
            </ul>
            
        </section>
        
        
        <?php include("content-reviews.php"); ?>
        
        
        <section class="container pre-footer">
            <div class="grid">
                <div class="col-md-6 col-bleed">
                    <h3>Suscríbete</h3>
                    <div class="mailchimp-wrapper">
                        <p>Infórmate de nuestras ofertas y novedades</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                            <form action="https://lagaviota.us18.list-manage.com/subscribe/post?u=68915ed9d82bfb3c16f251ef4&amp;id=795b8699be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                <div id="mc_embed_signup_scroll">
                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                    </div>
                                    <div class="mc-field-group">
                                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                                    </div>
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response"></div>
                                        <div class="response" id="mce-success-response"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div class="real-people" aria-hidden="true"><input type="text" name="b_ba996e4ee764e3745ef59df0b_b9fdbe7c75" tabindex="-1" value=""></div>
                                    <div class="p-accept">
                                    <input type="checkbox" id="gdpr_29941" name="gdpr[29941]" value="Y" class="av-checkbox gdpr"><span>He leído y acepto su <a href="https://www.lagaviota.com/politica-de-privacidad">política de privacidad</a></span>
                                    </div>
                                    <div class="clear">
                                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Ends MailChimp Signup Form -->
                    </div>
                </div>
                <div class="col-md-6 col-bleed">
                    <h3>Cómo llegar</h3>
                    <a href="https://g.page/gaviotaCAMPING?share" class="map" title="Ver el mapa de Google" target="_blank"><span>Ver en Google Maps</span></a>
                    <!--<img class="multi-logo" src="assets/images/google-map-la-gaviota.jpg" alt="Logos" width="1140" height="390">-->
                </div>
            </div>
        </section>
        
        
    </main>
    
    
<?php include("footer.php"); ?>
